package com.cloudcc.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrrcZhongcheApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrrcZhongcheApplication.class, args);
    }

}
