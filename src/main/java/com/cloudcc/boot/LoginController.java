package com.cloudcc.boot;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.cloudcc.boot.utils.util.MD5.MD5Encode;

@RestController
public class LoginController {
    public static String adminloginname;
    @Value("${system.userName}")
    public  void setUserName(String userName) {
        this.adminloginname = userName;
    }

    public static String url;
    @Value("${system.url}")
    public  void setUrl(String url) {
        this.url = url;
    }

    public static String loginurl;
    @Value("${system.loginurl}")
    public  void setLoginurl(String loginurl) {
        this.loginurl = loginurl;
    }

    public static String safetyMark;
    @Value("${system.safetyMark}")
    public  void setSafetyMark(String safetyMark) {
        this.safetyMark = safetyMark;
    }
    public static String clientId;
    @Value("${system.clientId}")
    public  void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public static String secretKey;
    @Value("${system.secretKey}")
    public  void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
    public static String orgId;
    @Value("${system.orgId}")
    public  void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @GetMapping("/getData")
    public Map<String, String> getData(String login_name, String login_pwd) throws Exception {
        Map<String, String> data = new HashMap<>();
        data.put("key", login_pwd+"=========="+("".equals(login_pwd)));

        if ("".equals(login_name) || "".equals(login_pwd)) {
            data.put("result", "false");
            data.put("msg", "请维护用户名、密码");
        } else {
            String adminAccessToken = gettoken(adminloginname, safetyMark);//获取TOKEN
            //查询CRM是否有数据
            JSONObject jsoncontent = new JSONObject();
            jsoncontent.put("serviceName", "cqlQueryWithLogInfo");
            jsoncontent.put("objectApiName", "ccuser");
            jsoncontent.put("expressions", "select * from ccuser where loginname='" + login_name + "'");

            String resultselectquery = operate(url+"openApi/common", jsoncontent.toString(),adminAccessToken);
            JSONObject select_result = JSONObject.fromObject(resultselectquery);
            String date = select_result.get("data") == null ? "" : select_result.getString("data");
            if (date.length() == 2) {//没有该域账号对应的用户
                data.put("result", "false");
                data.put("msg", "用户名或密码错误");
            } else {
                //查询账号密码
                JSONObject jsoncontent1 = new JSONObject();
                jsoncontent1.put("serviceName", "cqlQueryWithLogInfo");
                jsoncontent1.put("objectApiName", "tp_sys_user");
                login_pwd = MD5Encode (login_pwd);
                jsoncontent1.put("expressions", "select * from tp_sys_user where login_name='" + login_name + "' and pwd='"+login_pwd+"'");

                String resultselectpwdquery = operate(url+"openApi/common", jsoncontent1.toString(),adminAccessToken);
                JSONObject selectpwd_result = JSONObject.fromObject(resultselectpwdquery);
                String pwddate = selectpwd_result.get("data") == null ? "" : selectpwd_result.getString("data");
                JSONArray dataArray = new JSONArray();
                dataArray = JSONArray.fromObject(pwddate);
                if (dataArray.size() == 0) {
                    data.put("result", "false");
                    data.put("msg", "用户名或密码错误");
                } else {
                    JSONObject dataJson = dataArray.getJSONObject(0);
                    String isusing = dataJson.get("ISUSING")==null?"":dataJson.get("ISUSING").toString();
                    String safety_mark = dataJson.get("safety_mark")==null?"":dataJson.get("safety_mark").toString();
                    data.put("result", "false");
                    data.put("msg", isusing+"");
                    if ("0".equals(isusing)) {
                        data.put("result", "false");
                        data.put("msg", "用户未启用，请联系管理员");
                    } else {
                        String userAccessToken = gettoken(login_name,safety_mark);//获取TOKEN
                        data.put("result", "true");
                        data.put("msg", loginurl+"?binding="+userAccessToken);
                    }
                }
            }
        }
        return data;
    }
    public String gettoken(String login_name, String safety_mark) throws Exception {
        JSONObject jsoncontent = new JSONObject();
        jsoncontent.put("username", login_name);
        jsoncontent.put("safetyMark", safety_mark);
        jsoncontent.put("clientId", clientId);
        jsoncontent.put("secretKey", secretKey);
        jsoncontent.put("orgId", orgId);
        jsoncontent.put("grant_type", "password");
        URL tokenurl = null;
        String returnContent = "";
        try {
            tokenurl = new URL(url+"api/cauth/token");
            HttpURLConnection urlConnection = (HttpURLConnection) tokenurl.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.getOutputStream().write(jsoncontent.toString().getBytes("UTF-8"));
            urlConnection.connect();
            //获取状态码
            int code = urlConnection.getResponseCode();
            InputStream raw;
            if (code == 200) {
                InputStream in = urlConnection.getInputStream();
                raw = new BufferedInputStream(in);
            } else {
                raw = urlConnection.getErrorStream();// HttpURLConnection 才有getErrorStream方法
            }
            //放入缓存流
            //最后使用Reader接收
            Reader r = new InputStreamReader(raw);
            //打印输出
            int c;
            while((c = r.read())>0){
                returnContent +=(char)c;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = JSONObject.fromObject(returnContent);

        JSONObject dJson = JSONObject.fromObject(jsonObject.get("data").toString());
        String accessToken = dJson.get("accessToken").toString();
        return accessToken;
    }
    public static String operate(String url_str, String data_str, String accessToken) throws Exception {
        URL tokenurl = null;
        String returnContent = "";
        try {
            tokenurl = new URL(url_str);
            HttpURLConnection urlConnection = (HttpURLConnection) tokenurl.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.addRequestProperty("accessToken", accessToken);
            urlConnection.getOutputStream().write(data_str.toString().getBytes("UTF-8"));
            urlConnection.connect();
            //获取状态码
            int code = urlConnection.getResponseCode();
            InputStream raw;
            if (code == 200) {
                InputStream in = urlConnection.getInputStream();
                raw = new BufferedInputStream(in);
            } else {
                raw = urlConnection.getErrorStream();// HttpURLConnection 才有getErrorStream方法
            }
            //放入缓存流
            //最后使用Reader接收
            Reader r = new InputStreamReader(raw);
            //打印输出
            int c;
            while((c = r.read())>0){
                returnContent +=(char)c;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnContent;
    }
}
