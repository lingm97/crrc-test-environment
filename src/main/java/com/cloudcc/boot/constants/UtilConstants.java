package com.cloudcc.boot.constants;

/**
 * @author Administrator
 */
public interface UtilConstants {
    /**
     *
     * 公共常量
     *
     */
    public static class Public {
        /**
         *测试环境
         */
//        public static final String CLOUDCCIP = "http://172.28.188.171:55556/ccdomaingateway/apisvc";//测试环境
//        public static final String CLOUDCCIP_PORT = "http://172.28.188.171:55556/ccdomaingateway/apisvc";//测试环境
//        public static  final String xmylx_workflowId = "341";//项目预立项_流程ID
//        public static  final String xmlx_workflowId = "342";//项目立项_流程ID
//        public static  final String xmlx_xmht_tableDBName = "formtable_main_536_dt2";//项目立项_项目合同_子表表名
//        public static  final String xmlx_cbcs_tableDBName = "formtable_main_536_dt1";//项目立项_成本测算_子表表名
//        public static  final String htfxpg_workflowId = "360";//合同风险评估_流程ID
//        public static  final String contract_workflowId_fd = "352";//合同_流程ID  (注意：测试环境只有风电，没有山东公司）
//        public static  final String contract_xsnr_tableDBName_fd = "formtable_main_544_dt4";//合同_销售内容_子表表名
//        public static  final String contract_skjh_tableDBName_fd = "formtable_main_544_dt2";//合同_付款信息_子表表名
//        public static  final String contract_workflowId_gj = "352";//合同_流程ID
//        public static  final String contract_xsnr_tableDBName_gj = "formtable_main_544_dt4";//合同_销售内容_子表表名
//        public static  final String contract_skjh_tableDBName_gj = "formtable_main_544_dt2";//合同_付款信息_子表表名
//        public static  final String zbwjps_workflowId = "194";//招标文件评审_流程ID
//        public static  final String tbwjps_workflowId = "200";//投标文件评审_流程ID
//        public static  final String shsq_workflowId = "380";//上会申请_流程ID


        /**
         * 正式环境
         */
        public static final String CLOUDCCIP = "http://172.28.188.172:55555/ccdomaingateway/apisvc";//正式环境
        public static final String CLOUDCCIP_PORT = "http://172.28.188.172:55555/ccdomaingateway/apisvc";//正式环境
        public static  final String xmylx_workflowId = "284";//项目预立项_流程ID
        public static  final String xmlx_workflowId = "291";//项目立项_流程ID
        public static  final String xmlx_xmht_tableDBName = "formtable_main_350_dt2";//项目立项_项目合同_子表表名 (注意：OA只有风电在用）
        public static  final String xmlx_cbcs_tableDBName = "formtable_main_350_dt1";//项目立项_成本测算_子表表名 (注意：OA只有风电在用）
        public static  final String htfxpg_workflowId = "312";//合同风险评估_流程ID
        public static  final String contract_workflowId_fd = "278";//合同_流程ID  风电278  山东公司360(注意：OA流程版本更新时，流程ID会变)
        public static  final String contract_xsnr_tableDBName_fd = "formtable_main_139_dt4";//合同_销售内容_子表表名 风电139_dt4 山东公司416_dt4
        public static  final String contract_skjh_tableDBName_fd = "formtable_main_139_dt2";//合同_付款信息_子表表名 风电139_dt2 山东公司416_dt2
        public static  final String contract_workflowId_gj = "360";//合同_流程ID  风电278  山东公司360(注意：OA流程版本更新时，流程ID会变)
        public static  final String contract_xsnr_tableDBName_gj = "formtable_main_416_dt4";//合同_销售内容_子表表名 风电139_dt4 山东公司416_dt4
        public static  final String contract_skjh_tableDBName_gj = "formtable_main_416_dt4";//合同_付款信息_子表表名 风电139_dt2 山东公司416_dt2
        public static  final String zbwjps_workflowId = "362";//招标文件评审_流程ID
        public static  final String tbwjps_workflowId = "258";//投标文件评审_流程ID
        public static  final String shsq_workflowId = "345";//上会申请_流程ID

        //public static final String CLOUDCCIP = "https://www.tlc-crm.com.cn";//正式环境
        //public static final String CLOUDCCIP_PORT = "https://www.tlc-crm.com.cn";//正式环境
    }


}