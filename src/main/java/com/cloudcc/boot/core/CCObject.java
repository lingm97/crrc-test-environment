package com.cloudcc.boot.core;

import java.util.HashMap;

/**
 * @author T.han
 * @date 2023/8/17 16:18
 * @project CloudccNewPro
 * @Description //TODO
 */
public class CCObject extends HashMap {
    public static final String OBJECT_API = "CCObjectAPI";
    public static final String IS_SHARED = "isShared";

    public CCObject() {
        /* compiled code */
    }

    public CCObject(String ccobj) {
        /* compiled code */
        put("CCObjectAPI", ccobj);
    }

    public CCObject(String ccobj, String isShared) {
        /* compiled code */
        put("CCObjectAPI", ccobj);
        put("isShared", "isShared");
    }

    public void putSumValue(String sumValueKey, String value) {
        /* compiled code */
        put("sum" + sumValueKey, value);
    }

    public Object getSumValue(String sumValueKey) {
        /* compiled code */
        return get("sum" + sumValueKey);
    }

    public String getObjectApiName() {
        /* compiled code */
        return (String)get("CCObjectAPI");
    }


}
