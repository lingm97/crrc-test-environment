package com.cloudcc.boot.core;

/**
 * @author T.han
 * @date 2023/8/17 15:14
 * @project CloudccNewPro
 * @Description //TODO
 */
public class UserInfo {
    private String username;
    private String password;
    private String url;

    public UserInfo() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return url;
    }

    public void setUserId(String url) {
        this.url = url;
    }

}
