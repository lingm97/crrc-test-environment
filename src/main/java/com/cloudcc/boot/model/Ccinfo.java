package com.cloudcc.boot.model;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @Author: LGH
 * @Date: 2020/12/25 0025 15:58
 */
@Data
@Component
public class Ccinfo {
    private int id;
    private String name;
}
