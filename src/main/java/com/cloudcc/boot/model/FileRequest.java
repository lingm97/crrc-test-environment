package com.cloudcc.boot.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class FileRequest {
    private String relatedId;//记录ID
    private String fileName;//文件名称
    private String suffix;//文件后缀
    private MultipartFile file;//文件

    private String serviceName = "uploadFile";
}
