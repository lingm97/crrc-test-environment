package com.cloudcc.boot.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lin
 * 员工测试类
 */
@Data
@Component
@ConfigurationProperties(prefix = "system")
public class SystemInfo {
    /**userName*/
    private String userName;
    /**password*/
    private String password;
    /**url*/
    private String url;
    /**tokenkey*/
    private String tokenkey;
}
