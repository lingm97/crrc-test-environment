package com.cloudcc.boot.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lin
 * 员工测试类
 */
@Data
@Component
@ApiModel(description = "Rustful风格测试类")
@ConfigurationProperties(prefix = "user")
public class User {
    /**员工id*/
    @ApiModelProperty(value = "员工id",name = "id")
    private String userId;
    /**名字*/
    @ApiModelProperty(value = "员工姓名",name = "name",required = true)
    private String userName;
    /**密码*/
    private String password;
    /**手机号码*/
    private String phone;
}
