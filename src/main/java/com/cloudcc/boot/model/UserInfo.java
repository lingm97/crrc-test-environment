package com.cloudcc.boot.model;

import com.cloudcc.client.CCObject;
import org.springframework.stereotype.Service;

@Service
public class UserInfo extends CCObject{
	public String getUserId() {
		return "";
	}

	public String getRoleId() {
		return "";
	}
	
	public UserInfo (String username,String pwd) {
		this.put("userName", username);
		this.put("password", pwd);
	}
	
	public UserInfo() {
		
	}

	public String getProfileId() {
		return null;
	}
}
