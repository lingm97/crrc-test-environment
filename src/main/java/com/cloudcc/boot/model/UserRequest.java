package com.cloudcc.boot.model;

import lombok.Data;
import net.sf.json.JSONArray;

@Data
public class UserRequest {
    private JSONArray data;//传入数据
    private String processName;//流程名称
    private String recordID;//记录ID
    private String processType;//流程结果
}
