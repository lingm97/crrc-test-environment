package com.cloudcc.boot.oaservice;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import com.cloudcc.boot.model.UserRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import static com.cloudcc.boot.core.CCService.userInfo;

@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "" ,tags = {"Rustful"},description = "")
public class approvalProcess {
    @Value("${system.localurl:''}")
    public String localurl;
    /**
     * author : 黄涛 on 2022-11-17
     * 客户投诉新建及更新
     */
    @ResponseBody
    @RequestMapping(value = "/process_receive", produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "")
    public String process_receive(@RequestBody UserRequest userRequest, HttpServletRequest request) throws Exception {
        String userRquestStr = JSONObject.fromObject(userRequest).toString();//入参
        log.info("process_receive请求参数{}", userRquestStr);
        JSONObject returnJson = new JSONObject();
        CCService cs = new CCService(userInfo);

        CCObject interfacerecordObj = new CCObject("interfacerecord");//接口同步记录
        interfacerecordObj.put("url", localurl + "/api/saveProcess");//url
        interfacerecordObj.put("crcs", userRquestStr);//传入参数
        try {
            String processName = userRequest.getProcessName() == null ? "" : userRequest.getProcessName();//流程名称
            String recordID = userRequest.getRecordID() == null ? "" : userRequest.getRecordID();//主表记录ID
            String objectApiName = "";//对象名
            String recordApiName = "";//主详字段名
            if ("招标文件评审".equals(processName)){
                objectApiName = "zbwjps_spbz";
                recordApiName = "tbgl";
            }else if ("投标文件评审".equals(processName)){
                objectApiName = "tbwjps_spbz";
                recordApiName = "tbgl";
            }else if ("投标决策申请".equals(processName)){
                objectApiName = "tbjcsq_spbz";
                recordApiName = "tbgl";
            }else if ("项目预立项".equals(processName)){
                objectApiName = "xmylx_spbz";
                recordApiName = "xmylx";
            }else if ("项目立项".equals(processName)){
                objectApiName = "xmlx_spbz";
                recordApiName = "xmlx";
            }else if ("合同风险评估".equals(processName)){
                objectApiName = "htfxpg_spbz";
                recordApiName = "htfxpg";
            }else if ("合同".equals(processName)){
                objectApiName = "ht_spbz";
                recordApiName = "ht";
            }
            //清空历史数据
            String delete_sql = "update "+objectApiName+" set is_deleted=1 where "+recordApiName+" = '"+recordID+"'";
//            cs.cqlQuery(objectApiName,delete_sql);
            //数据处理
            JSONArray dataArray = userRequest.getData();//date数据
            JSONArray putArray = new JSONArray();
            for(int i=0;i<dataArray.size();i++){
                JSONObject caseObj = dataArray.getJSONObject(i);
                caseObj.put(recordApiName,recordID);//添加主详关系字段
                putArray.add(caseObj);
            }
            String putArrayStr = putArray.toString();
            //接口推送
            JSONObject parObj = new JSONObject();
            parObj.put("serviceName", "insert"); //服务名
            parObj.put("objectApiName", objectApiName); //对象名
            parObj.put("data", putArrayStr); //查询条件
///            System.out.println("parObj = " + parObj);
            String relStr = cs.CCPost(cs.path, parObj.toString());

            returnJson = JSONObject.fromObject(relStr);

            interfacerecordObj.put("zxzt", "处理成功");//执行状态
        } catch (Exception e) {
            returnJson.put("result", "false");
            returnJson.put("returnInfo", e + "");
            returnJson.put("returnCode", "-1");

            interfacerecordObj.put("zxzt", "处理失败");//执行状态
        } finally {
            interfacerecordObj.put("fhcs", returnJson.toString());//返回参数
//            log.error("返回参数{}", returnJson.toString());
            //写入数据 newObj
            ServiceResult sr = cs.insert(interfacerecordObj);
        }
        return returnJson+"";
    }
}
