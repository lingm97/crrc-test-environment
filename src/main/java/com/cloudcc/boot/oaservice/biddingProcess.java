package com.cloudcc.boot.oaservice;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import com.cloudcc.boot.model.UserRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static com.cloudcc.boot.core.CCService.userInfo;

@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "" ,tags = {"Rustful"},description = "")
public class biddingProcess {
    @Value("${system.localurl:''}")
    public String localurl;
    /**
     * author : 黄涛 on 2024-9-11
     * 招标/投标/上会申请审批状态更新接口
     */
    @ResponseBody
    @RequestMapping(value = "/bidding_receive", produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "")
    public String bidding_receive(@RequestBody UserRequest userRequest, HttpServletRequest request) throws Exception {
        String userRquestStr = JSONObject.fromObject(userRequest).toString();//入参
        log.info("bidding_receive请求参数{}", userRquestStr);
        JSONObject returnJson = new JSONObject();
        CCService cs = new CCService(userInfo);

        CCObject interfacerecordObj = new CCObject("interfacerecord");//接口同步记录
        interfacerecordObj.put("url", localurl + "/api/bidding_receive");//url
        interfacerecordObj.put("crcs", userRquestStr);//传入参数
        try {
            String processName = userRequest.getProcessName() == null ? "" : userRequest.getProcessName();//流程名称
            String recordID = userRequest.getRecordID() == null ? "" : userRequest.getRecordID();//主表记录ID
            String processType = userRequest.getProcessType() == null ? "" : userRequest.getProcessType();//流程结果
            String errorMsg = "";
            if (processName.length()<=0){
                errorMsg += "【processName】";
            }
            if (recordID.length()<=0){
                errorMsg += "【recordID】";
            }
            if (processType.length()<=0){
                errorMsg += "【processType】";
            }
            if (errorMsg.length()>0){
                returnJson.put("result", "false");
                returnJson.put("returnInfo", errorMsg+"参数不能为空！");
                returnJson.put("returnCode", "-1");
                return returnJson.toString();
            }
            String updateSql = "update tendermanagement set spzt='"+processName+processType+"' where id = '"+recordID+"'";
            List<CCObject> rel_cqlQuery = cs.cqlQuery("tendermanagement",updateSql);

            returnJson.put("result", "true");
            returnJson.put("returnInfo", "");
            returnJson.put("returnCode", "1");

            interfacerecordObj.put("zxzt", "处理成功");//执行状态
        } catch (Exception e) {
            returnJson.put("result", "false");
            returnJson.put("returnInfo", e + "");
            returnJson.put("returnCode", "-1");

            interfacerecordObj.put("zxzt", "处理失败");//执行状态
        } finally {
            interfacerecordObj.put("fhcs", returnJson.toString());//返回参数
//            log.error("返回参数{}", returnJson.toString());
            //写入数据 newObj
            ServiceResult sr = cs.insert(interfacerecordObj);
        }
        return returnJson+"";
    }
}
