package com.cloudcc.boot.oaservice;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import com.cloudcc.boot.model.UserRequest;
import com.cloudcc.boot.web.OA_Operate;
import com.cloudcc.boot.web.OA_Token;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.cloudcc.boot.core.CCService.userInfo;

@Controller
@RequestMapping(value = "/api")
@Slf4j
public class tbjcsqClass {
    @Value("${system.localurl:''}")
    public String localurl;
    @Value("${system.url:''}")
    public String url;
    @Value("${system.oaurl:''}")
    public String oaurl;
    @Autowired
    public OA_Token oa;
    @Autowired
    private OA_Operate oa_operate;
    /**
     * author : 黄涛 on 2024-07-08
     * 项目预立项推送OA
     */
    @ResponseBody
    @RequestMapping(value = "/tendermanagement_tbjcsqoaid_push", produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "")
    public String tendermanagement_tbjcsqoaid_push(UserRequest userRequest, HttpServletRequest request) throws Exception {
        String userRquestStr = JSONObject.fromObject(userRequest).toString();//入参
        log.info("tendermanagement_tbjcsqoaid_push请求参数{}", userRquestStr);
        JSONObject returnJson = new JSONObject();
        CCObject interfacerecordObj = new CCObject("interfacerecord");//接口同步记录
        CCService cs = new CCService(userInfo);

        try {
            String recordID = userRequest.getRecordID() == null ? "" : userRequest.getRecordID();//主表记录ID
            String objectApiName = "tendermanagement";//对象名
            String workflowId = "193";//流程ID
            String isnextflow = "1";//isnextflow ：新建流程是否默认提交到第二节点，可选值为[0 ：不流转 1：流转 (默认)]
            //查询oa经办人ID
            String oa_user_id = oa_operate.getOaUserId(objectApiName,recordID);
            //处理主表
            List<JSONObject> mainList = oa_operate.dealOAMainParam(objectApiName,recordID,"投标决策申请");
            if(CollectionUtils.isEmpty(mainList)){
                returnJson.put("code", "ERROR");
                returnJson.put("errMsg", "主表数据为空！");
                return returnJson.toString();
            }
            log.info("===================================");
            log.info("list==size{}==={}", mainList.size(), JSONArray.fromObject(mainList).toString());
            JSONObject param = new JSONObject();
            param.put("mainData",mainList);
            JSONObject otherParams = new JSONObject();
            otherParams.put("isnextflow",isnextflow);
            param.put("otherParams",otherParams);
            param.put("workflowId",workflowId);
            //TODO 调接口传 param
            String oa_path =oaurl+"/api/workflow/paService/doCreateRequest";//OA创建流程接口
            log.info("osSendPost-->oa_path参数：{}",oa_path);
            log.info("osSendPost-->oa_user_id参数：{}",oa_user_id);
            log.info("osSendPost-->param参数：{}",param.toString());
            String data = oa.osSendPost(oa_path,oa_user_id,param.toString());
            log.info("========================");
            log.info("OA返回参数：：" + data);
            if (data.length()>0){
                returnJson = JSONObject.fromObject(data);
            }else {
                returnJson.put("code", "ERROR");
                returnJson.put("errMsg", "OA接口无返回");
            }
            interfacerecordObj.put("url", oa_path);//url
            interfacerecordObj.put("crcs", param.toString());//传入参数
            interfacerecordObj.put("zxzt", "处理成功");//执行状态
        } catch (Exception e) {
            returnJson.put("code", "ERROR");
            returnJson.put("errMsg", e + "");

            log.info("========================");
            log.info("报错信息："+e.toString());

            interfacerecordObj.put("zxzt", "处理失败");//执行状态
        } finally {
            interfacerecordObj.put("fhcs", returnJson.toString());//返回参数
            //写入数据 newObj
            ServiceResult sr = cs.insert(interfacerecordObj);
        }
        return returnJson.toString();
    }
}
