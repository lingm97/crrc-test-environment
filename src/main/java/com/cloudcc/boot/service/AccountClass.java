package com.cloudcc.boot.service;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.UserInfo;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.List;

/**
 * @author T.han
 * @date 2024/1/30 18:04
 * @project CloudccNewPro
 * @Description //TODO
 */
public class AccountClass {

    private final CCService cs;
    private final UserInfo userInfo;

    public AccountClass(UserInfo userInfo) {
        this.userInfo = userInfo;
        cs = new CCService(userInfo);
    }


    /**
     * author：LGM on 2023-07-04
     * 客户列表页面展示，全部客户数据查询，注意：dataJSONObject的key需要与表头设置的key对应
     * 参数：status（状态），返回类型：JSONObject
     */
    public JSONObject queryAccount() throws Exception {
        JSONObject rtninfo = new JSONObject();
        JSONArray dataList = new JSONArray();
        try {
            String sql = "select name,leixing,hangye,id,grade from Account where is_deleted='0' ";
            List<CCObject> accountList = cs.cqlQuery("Account", sql);
            for (CCObject item : accountList) {
                JSONObject data = new JSONObject();
                data.put("accountname", item.get("name"));//客户名称
                data.put("type", item.get("leixing"));//类型
                data.put("industry", item.get("hangye"));//行业
                data.put("grade", item.get("grade"));//分级
                data.put("id", item.get("id"));
                dataList.add(data);

            }
        } catch (Exception e) {
            throw e;
        }
        rtninfo.put("status", true);
        rtninfo.put("contactList", dataList.toString());
        return rtninfo;
    }

    /**
     * author：LGM on 2023-07-04
     * 客户详细页面展示，根据客户id查询该客户数据
     * 参数：status（状态），返回类型：JSONObject
     */
    public JSONObject queryAccount(String accountId) throws Exception {
        JSONObject rtninfo = new JSONObject();
        try {
            String sql = "select name,leixing,hangye,fenji,id from Account where id='" + accountId + "' and is_deleted='0' ";
            List<CCObject> accountList = cs.cqlQuery("Account", sql);
            if (accountList.size() > 0) {
                String accountname = accountList.get(0).get("name") == null ? "" : accountList.get(0).get("name").toString();//客户名称
                String type = accountList.get(0).get("leixing") == null ? "" : accountList.get(0).get("leixing").toString();//类型
                String industry = accountList.get(0).get("hangye") == null ? "" : accountList.get(0).get("hangye").toString();//行业
                String grade = accountList.get(0).get("fenji") == null ? "" : accountList.get(0).get("fenji").toString();//分级

                rtninfo.put("accountname", accountname);//客户名称
                rtninfo.put("type", type);//类型
                rtninfo.put("industry", industry);//行业
                rtninfo.put("grade", grade);//分级
                rtninfo.put("id", accountId);

            }
        } catch (Exception e) {
            throw e;
        }
        rtninfo.put("status", true);
        return rtninfo;
    }

    /**
     * author：LGM on 2023-07-04
     * 更新客户，根据客户id
     * 参数：status（状态），返回类型：JSONObject
     */
    public JSONObject AccountUpdate(String accountname, String type, String industry, String grade, String accountId) throws Exception {
        JSONObject rtninfo = new JSONObject();
        try {
            List<CCObject> accountList = cs.cquery("Account", "id='" + accountId + "'");
            if (accountList.size() > 0) {
                accountList.get(0).put("name", accountname);//客户名称
                accountList.get(0).put("leixing", type);//类型
                accountList.get(0).put("hangye", industry);//行业
                accountList.get(0).put("fenji", grade);//分级
                cs.update(accountList.get(0));
            }
        } catch (Exception e) {
            throw e;
        }
        rtninfo.put("status", true);
        return rtninfo;
    }

    /**
     * author：LGM on 2023-07-04
     * 删除客户，根据客户id
     * 参数：status（状态），返回类型：JSONObject
     */
    public JSONObject AccountDelete(String accountId) throws Exception {
        JSONObject rtninfo = new JSONObject();
        try {
            List<CCObject> accountList = cs.cquery("Account", "id='" + accountId + "'");
            if (accountList.size() > 0) {
                cs.delete(accountList.get(0));
            }
        } catch (Exception e) {
            throw e;
        }
        rtninfo.put("status", true);
        return rtninfo;
    }
}






