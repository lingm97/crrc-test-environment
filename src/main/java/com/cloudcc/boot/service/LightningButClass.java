package com.cloudcc.boot.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.UserInfo;

import java.util.List;

/**
 * @author T.han
 * @date 2024/1/30 15:00
 * @project CloudccNewPro
 * @Description //TODO
 */
public class LightningButClass {
    private static UserInfo userInfo;
    private final CCService cs;

    public LightningButClass(UserInfo userInfo) {
        cs = new CCService(userInfo);
        LightningButClass.userInfo = userInfo;
    }

    public static void main(String[] args) throws Exception {
        LightningButClass ta = new LightningButClass(userInfo);
        JSONObject rntObj = ta.QueryData("爱妙");
        System.out.println("rnt: " + rntObj);


    }

    /**
     * 刷新当前页面
     *
     * @param recordid 数据id
     * @return
     * @throws
     */
    public JSONObject demo1(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "refresh");
        return rtnObj;
    }

    /**
     * 跳转链接，此时需传URL参数
     *
     * @param recordid
     * @return
     * @throws Exception
     */
    public JSONObject demo2(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "redirect");
        rtnObj.put("mode", "now");
        rtnObj.put("url", "/#/commonObjects/detail/" + recordid + "/DETAIL");
        return rtnObj;
    }

    /**
     * 弹出消息提示
     *
     * @param recordid
     * @return
     * @throws Exception
     */
    public JSONObject demo3(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "alert");
        rtnObj.put("message", "弹出消息提示的内容" + recordid);
        return rtnObj;
    }

    /**
     * 先弹窗提示，并自动刷新当前链接页面
     *
     * @param recordid
     * @return
     * @throws Exception
     */
    public JSONObject demo4(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "alert_refresh");
        rtnObj.put("message", "弹出消息提示的内容" + recordid);
        return rtnObj;
    }

    /**
     * 先弹窗提示，在执行url地址，此时应传mode参数，
     *
     * @param recordid
     * @return
     * @throws Exception
     */
    public JSONObject demo5(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "alert_redirect");
        rtnObj.put("message", "弹出消息提示的内容" + recordid);
        rtnObj.put("mode", "now");
        rtnObj.put("url", "/#/commonObjects/detail/" + recordid + "/DETAIL");
        return rtnObj;
    }


    public JSONObject demo6(String recordid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "alert_redirect");
        //{"refresh":"刷新当前页面",
        // "redirect":"跳转链接，此时需传URL参数"
        // ,"alert":"弹出消息提示"
        // ,"alert_refresh":"先弹窗提示，并自动刷新当前链接页面"
        // ,"alert_redirect":"先弹窗提示，在执行url地址，此时应传mode参数"
        // }
        rtnObj.put("message", "弹出消息提示的内容" + recordid); //弹出消息提示
        rtnObj.put("messageType", "success");  //提示消息类型
        //{"success":"成功类型的消息"
        // ,"info":"消息提示的文案"
        // ,"warning":"警告提示的消息"
        // ,"error":"错误 类型的消息"
        // }
        rtnObj.put("mode", "now"); // now：当前页面执行url new:在新标签页面打开url
        rtnObj.put("url", "/#/commonObjects/detail/" + recordid + "/DETAIL"); //跳转的URL地址
        return rtnObj;
    }

    public JSONObject deom7(String ids, String viwid) throws Exception {
        JSONObject rtnObj = new JSONObject();
        rtnObj.put("action", "alert_refresh");
        rtnObj.put("message", "获取id为：" + ids + "==视图id为：" + viwid);
        rtnObj.put("messageType", "info");
        rtnObj.put("mode", "now");
        return rtnObj;
    }

    /**
     * 查询方法测试方法
     *
     * @param val
     * @return
     * @throws Exception
     */
    public JSONObject QueryData(String val) throws Exception {
        JSONObject rtnObj = new JSONObject();
        String send = "1=1";
        if (!"".equals(val)) {
            send = "name like '%" + val + "%'";
        }
        List<CCObject> khlist = cs.cquery("Account", send);
        if (khlist.size() > 0) {
            JSONArray kharry = new JSONArray();
            for (CCObject ac : khlist) {
                JSONObject acObj = new JSONObject();
                String name = ac.get("name") == null ? "" : ac.get("name") + "";//名称
                String fenji = ac.get("fenji") == null ? "" : ac.get("fenji") + "";//分级
                String khxxdz = ac.get("khxxdz") == null ? "" : ac.get("khxxdz") + "";//地址
                String id = ac.get("id") == null ? "" : ac.get("id") + "";//记录ID
                acObj.put("name", name);
                acObj.put("fenji", fenji);
                acObj.put("khxxdz", khxxdz);
                acObj.put("id", id);
                kharry.add(acObj);
            }
            rtnObj.put("code", "0");
            rtnObj.put("msg", "成功！");
            rtnObj.put("data", kharry.toString());
        } else {
            rtnObj.put("code", "1");
            rtnObj.put("msg", "未查询到数据");
            rtnObj.put("data", "");
        }
        return rtnObj;
    }


    public JSONObject queryDate(String val) throws Exception {
        JSONObject rtnObj = new JSONObject();
        String send = "1=1";
        if (!"".equals(val)) {
            send = "id = '" + val + "'";
        }
        List<CCObject> khlist = cs.cquery("jdsq", send);
        if (khlist.size() > 0) {
            JSONArray kharry = new JSONArray();
            for (CCObject ac : khlist) {
                JSONObject acObj = new JSONObject();
                String name = ac.get("name") == null ? "" : ac.get("name") + "";//名称

                String id = ac.get("id") == null ? "" : ac.get("id") + "";//记录ID
                acObj.put("name", name);

                acObj.put("id", id);
                kharry.add(acObj);
            }
            rtnObj.put("code", "0");
            rtnObj.put("msg", "成功！");
            rtnObj.put("data", kharry.toString());
        } else {
            rtnObj.put("code", "1");
            rtnObj.put("msg", "未查询到数据");
            rtnObj.put("data", "");
        }
        return rtnObj;
    }


    public JSONArray findUser(String name, String loginname) {
        JSONArray ret = new JSONArray();
        String sql = "select name,id,loginname,phone,department  from ccuser  where 1=1 ";
        if (!"".equals(name)) {
            sql += " and name like '%" + name + "%'";
        }
        if (!"".equals(loginname)) {
            sql += " and loginname='%" + loginname + "%'";
        }
        List<CCObject> userList = cs.cqlQuery("", sql);

        JSONArray data = JSONObject.parseArray(JSONObject.toJSONString(userList));

        return data;
    }

}
