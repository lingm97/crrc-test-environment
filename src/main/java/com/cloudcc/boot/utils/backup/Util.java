package com.cloudcc.boot.utils.backup;

import com.cloudcc.boot.model.UserInfo;
import net.sf.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Util {

	public static String invokeCrm(UserInfo uInfo,String env){
		JSONObject jsonmsg = new JSONObject();
//		DesEncoder de = DesEncoder.getInstance();
		/*CCService cs = new CCService(uInfo,env);


	//触发器
			List<CCObject> triggers= cs.cqlQuery("tp_sys_trigger","select o.label, a.* from tp_sys_trigger a left join tp_sys_object o on a.target_object_id = o.id where o.is_deleted=0");
	//类
			List<CCObject> fags= cs.cqlQuery("tp_sys_fag","select * from tp_sys_fag");
	//定时类
			List<CCObject> ccpeaks= cs.cqlQuery("tp_sys_ccpeak","select * from tp_sys_ccpeak");
	//页面
			List<CCObject> visualPages= cs.cqlQuery("tp_sys_visualpage","select f.name as foldername,a.* from tp_sys_visualpage a left join tp_sys_folder f on a.folderid=f.id");
	//按钮和链接
			List<CCObject> buttons= cs.cqlQuery("tp_sys_button","select o.label as objname, a.* from tp_sys_button a left join tp_sys_object o on a.obj_id = o.id where category='CustomButton' and  o.is_deleted=0");
	//主页组件
			List<CCObject> components= cs.cqlQuery("tp_sys_mainpage_component","select * from tp_sys_mainpage_component");
	//表达式、公式字段
			List<CCObject> schemetables= cs.cqlQuery("tp_sys_schemetable","select o.label,o.schemetable_name,t.* from tp_sys_schemetable t left join tp_sys_object o on t.schemetable_id=o.id where t.expression is not null or t.execute_expression is not null and o.is_deleted=0");
	//验证规则
		List<CCObject> validateRules= cs.cqlQuery("tp_sys_validaterule ","select o.label,o.schemetable_name,t.* from tp_sys_validaterule t left join tp_sys_object o on t.obj_id=o.id where t.isactive ='true' and   o.is_deleted=0");

		jsonmsg.put("triggers", triggers);
		jsonmsg.put("fags", fags);
		jsonmsg.put("ccpeaks", ccpeaks);
		jsonmsg.put("visualPages", visualPages);
		jsonmsg.put("buttons", buttons);
		jsonmsg.put("components", components);
		jsonmsg.put("schemetables", schemetables);
		jsonmsg.put("validateRules", validateRules);*/
		 return jsonmsg.toString();
	}

	public static String getBinding(String env, String username, String password) {
		String location = env+ "/distributor.action?serviceName=clogin&userName="+username+"&password="+password;
		try {
			// 一，分解 Map 对象
			URL url = new URL(location);
			System.out.println("发送数据：" + location);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(30000);
			con.setConnectTimeout(10000);
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			//con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			//con.setRequestProperty("Cookie", "JSESSIONID=" + binding + ";");
			con.connect();
			InputStream inStream = con.getInputStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			byte[] resdata = outStream.toByteArray();// 网页的二进制数据
			outStream.close();
			inStream.close();
			String result = new String(resdata);
//			System.out.println("收到返回：" + result);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}



}
