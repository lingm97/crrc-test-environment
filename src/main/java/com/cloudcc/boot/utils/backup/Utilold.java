package com.cloudcc.boot.utils.backup;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Utilold {
	public static String invokeCrm(String env, String name, String binding, String params) {
		String location = env+"/customize/page/yLV36c130/" + name;
//		String location = env+"/customize/page/1426tBCYk/" + name;

		try {
			// 一，分解 Map 对象
			URL url = new URL(location + "?" + params);
			System.out.println("发送数据：" + location + "?" + params);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(30000);
			con.setConnectTimeout(10000);
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			con.setRequestProperty("Cookie", "JSESSIONID=" + binding + ";");
			con.connect();
			InputStream inStream = con.getInputStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			byte[] resdata = outStream.toByteArray();// 网页的二进制数据
			outStream.close();
			inStream.close();
			String result = new String(resdata,"utf-8");
//			String result = new String(resdata.getByte("ISO-8859-1"),"utf-8");
//			System.out.println("收到返回：" + result);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getBinding(String env, String username, String password) {
		String location = env+ "/distributor.action?serviceName=clogin&userName="+username+"&password="+password;
		try {
			// 一，分解 Map 对象
			URL url = new URL(location);
			System.out.println("发送数据：" + location);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(30000);
			con.setConnectTimeout(10000);
			con.setDoOutput(true);
			con.setRequestMethod("GET");
			//con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			//con.setRequestProperty("Cookie", "JSESSIONID=" + binding + ";");
			con.connect();
			InputStream inStream = con.getInputStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, len);
			}
			byte[] resdata = outStream.toByteArray();// 网页的二进制数据
			outStream.close();
			inStream.close();
			String result = new String(resdata);
//			System.out.println("收到返回：" + result);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}



}
