package com.cloudcc.boot.utils.demo;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.boot.utils.util.DBConnection;
import com.cloudcc.boot.utils.util.DataManagement;
import com.cloudcc.client.CCObject;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
@Slf4j
public class ImportToAccount {
	ResourceBundle prb = null;
	String userName = null;//
	String pwd = null;
	// 记录日志
	Map<String,JSONObject> acctMap = new ConcurrentHashMap<String, JSONObject>();
	Map<String,String> usrMap = new ConcurrentHashMap<String, String>();
	
	public ImportToAccount() {
		prb = PropertyResourceBundle.getBundle("conf");// 获取配置文件
		userName = prb.getString("userName");
		pwd = prb.getString("password");
	}
	public void importToCloudcc() {
		
		SimpleDateFormat mdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.SSS");
		Date sysdate = new Date();
		ExecutorService es = Executors.newFixedThreadPool(5);//数据同步线程池.
		log.info("importToCloudcc is called on " + mdf.format(sysdate) );
		try {
			SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
			log.info("获取数据库连接dbconn");
			DBConnection dbconn = new DBConnection();
			Connection conn = DBConnection.getConnection();
			log.info("获取数据库连接dbconn[完成]");
			UserInfo uInfo = new UserInfo(this.userName, this.pwd);
			// 查询日度销售数据
			int pagesize = 200;
			int totalCount = 0;
			// 查询status=1的产品日度销售数据，这部分为更新后需要删除的部分
			CCService cs = new CCService(uInfo);
			String sql = "select count(name) cnt from account_temp";
			log.info(sql);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				totalCount = rs.getInt("cnt");
			}
			int mod = totalCount % pagesize;
			int pageCount = 0;
	
			if (mod == 0) {
				pageCount = totalCount / pagesize;
			} else {
				pageCount = totalCount / pagesize + 1;
			}
			
			log.info("totalCount:"+totalCount+",pageCount:"+pageCount);
			for (int i = 0; i < pageCount; i++) {
				List<JSONObject> datalist = new ArrayList<JSONObject>();
				sql = "select * from account_temp t0";
				rs = dbconn.pageWithLimit(i + 1, pagesize, conn, sql);
				while (rs.next()) {
					JSONObject account = new JSONObject();
					account.put("name", rs.getString("name"));
					account.put("acountnum", rs.getString("acountnum"));
					account.put("businesscustomerid", rs.getString("bcustno"));
					account.put("billingtype", rs.getString("billingtype"));
					account.put("city", rs.getString("city"));
					account.put("contactname", rs.getString("contactname"));
					account.put("contractcustomerid", rs.getString("contractcustomerid"));
					account.put("customercategory", rs.getString("customercategory"));
					account.put("detailaddress", rs.getString("detailaddress"));
					account.put("telephone", rs.getString("telephone"));
					account.put("employeeamount", rs.getInt("employeeamount"));
					account.put("industry", rs.getString("industry"));
					account.put("lp", rs.getInt("lp"));
					account.put("mfp", rs.getInt("mfp"));
					account.put("ownernum", rs.getString("ownernum"));
					account.put("paymentdays", rs.getString("paymentdays"));
					account.put("province", rs.getString("province"));
					account.put("subindustry", rs.getString("subindustry"));
					account.put("tdv", rs.getInt("tdv"));
					account.put("recordtype", rs.getString("recordtype"));
					account.put("district", rs.getString("district"));
					account.put("employeescalecode", rs.getString("employeescalecode"));
					datalist.add(account);
				}
			
				if (datalist.size() > 0) {
					//异步调用执行数据同步
					DataManagement dmg = new DataManagement("batchInsert","account_tmp", datalist,cs);
					es.submit(dmg);
				}
				log.info("page"+i+"submited:"+datalist.size());
			}
			rs.close();
			conn.close();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public void createAccount() {
		SimpleDateFormat mdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.SSS");
		Date sysdate = new Date();
		ExecutorService es = Executors.newFixedThreadPool(5);//数据同步线程池.
		log.info("importToCloudcc is called on " + mdf.format(sysdate) );
		try {
			UserInfo uInfo = new UserInfo(this.userName, this.pwd);
			CCService cs = new CCService(uInfo);
			String sql = "select * from account_tmp t where not exists (select id from account t0 where t0.acountnum=t.acountnum)";
			List<CCObject> clist = cs.cqlQuery("Account", sql);
			int pagesize = 200;
			int totalCount = 1266;
			// 查询status=1的产品日度销售数据，这部分为更新后需要删除的部分
//			log.info(sql);
//			int mod = totalCount % pagesize;
//			int pageCount = 0;
//			if (mod == 0) {
//				pageCount = totalCount / pagesize;
//			} else {
////				pageCount = totalCount / pagesize + 1;
//			}
			
			List<JSONObject> datalist = new ArrayList<JSONObject>();
			for (int i = 0; i < clist.size(); i++) {
				CCObject acct = clist.get(i);
//				sql = "select * from account_tmp t where not exists (select id from account t0 where t0.acountnum=t.acountnum) limit "+(pageCount*pagesize)+",1000";
//				clist = cs.cqlQuery("Account", sql);
				JSONObject jo = new JSONObject();
				jo.put("name", acct.get("name"));
				jo.put("recordtype", acct.get("recordtype"));
				jo.put("telephone", acct.get("telephone"));
				jo.put("province", acct.get("province"));
				jo.put("mfp", acct.get("mfp"));
				jo.put("subindustry", acct.get("subindustry"));
				jo.put("billingtype", acct.get("billingtype"));
				jo.put("ownernum", acct.get("ownernum"));
				jo.put("employeeamount", acct.get("employeeamount"));
				jo.put("detailaddress", acct.get("detailaddress"));
				jo.put("customercategory", acct.get("customercategory"));
				jo.put("businesscustomerid", acct.get("bcustno"));
				jo.put("contractcustomerid", acct.get("contractcustomerid"));
				jo.put("industry", acct.get("industry"));
				jo.put("contactname", acct.get("contactname"));
				jo.put("city", acct.get("city"));
				jo.put("lp", acct.get("lp"));
				jo.put("acountnum", acct.get("acountnum"));
				jo.put("tdv", acct.get("tdv"));
				jo.put("employeescalecode", acct.get("employeescalecode"));
				jo.put("district", acct.get("district"));
				jo.put("paymentdays", acct.get("paymentdays"));
				datalist.add(jo);
				if (datalist.size() == 200) {
					//异步调用执行数据同步
					DataManagement dmg = new DataManagement("batchInsert","Account", datalist,cs);
					es.submit(dmg);
					datalist = new ArrayList<JSONObject>();
				}
			}
			DataManagement dmg = new DataManagement("batchInsert","Account", datalist,cs);
			es.submit(dmg);
			datalist = new ArrayList<JSONObject>();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	public String nvlVal(String val) {
		if (val==null) {
			return "";
		} else {
			return val;
		}
	}
	public static void main(String[] args) {
		ImportToAccount ita = new ImportToAccount();
		ita.createAccount();
		//ita.importToCloudcc();
	}
}
