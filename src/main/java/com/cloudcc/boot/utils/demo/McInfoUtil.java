package com.cloudcc.boot.utils.demo;

import com.cloudcc.boot.utils.wsdl.*;
import net.sf.json.JSONObject;
import org.apache.axis.client.Service;

import java.net.URL;
import java.util.Map;

public class McInfoUtil {
	//客户接口专用
	public JSONObject sychAccountInfo (String endpoint , String parameter) {
		JSONObject rstInfo = new JSONObject();
		String response="";
		try {
			    Service service = new Service();
				URL url = new URL(endpoint);
				TIPTOPServiceGateWayBindingStub tbs = new TIPTOPServiceGateWayBindingStub(url,service);
				GetKHaimm221Request_GetKHaimm221Request parameters = new GetKHaimm221Request_GetKHaimm221Request();
				parameters.setRequest(parameter);
				GetKHaimm221Response_GetKHaimm221Response rsp = tbs.getKHaimm221(parameters);
				response=rsp.getResponse();
			System.out.println("result"+response.toString());
			Xmlparse t=new Xmlparse();
			Map xmlMap=t.parseXml2Map(response.toString());
			rstInfo.put("result", xmlMap.get("result"));
			rstInfo.put("returnInfo", xmlMap.get("returnInfo"));
			rstInfo.put("returnCode", xmlMap.get("returnCode"));
		} catch (Exception e) {
			rstInfo.put("result", false);
			rstInfo.put("returnInfo", e.toString());
			rstInfo.put("returnCode", "");
		}
		return rstInfo;
	}
	//合同接口专用
	public JSONObject sychContractInfo (String endpoint , String parameter) {
		JSONObject rstInfo = new JSONObject();
		String response="";
		try {
			Service service = new Service();
			URL url = new URL(endpoint);
			TIPTOPServiceGateWayBindingStub tbs = new TIPTOPServiceGateWayBindingStub(url,service);
			GetXSDDaxmt410Request_GetXSDDaxmt410Request parameters = new GetXSDDaxmt410Request_GetXSDDaxmt410Request();
			parameters.setRequest(parameter);
			GetXSDDaxmt410Response_GetXSDDaxmt410Response rsp = tbs.getXSDDaxmt410(parameters);
			response=rsp.getResponse();
			System.out.println("result"+response.toString());
			Xmlparse t=new Xmlparse();
			Map xmlMap=t.parseXml2Map(response.toString());
			rstInfo.put("result", xmlMap.get("result"));
			rstInfo.put("returnInfo", xmlMap.get("returnInfo"));
			rstInfo.put("returnCode", xmlMap.get("returnCode"));
		} catch (Exception e) {
			rstInfo.put("result", false);
			rstInfo.put("returnInfo", e.toString());
			rstInfo.put("returnCode", "");
		}
		return rstInfo;
	}
	//发货接口专用
	public JSONObject sychFhdInfo (String endpoint , String parameter) {
		JSONObject rstInfo = new JSONObject();
		String response="";
		try {
			Service service = new Service();
			URL url = new URL(endpoint);
			TIPTOPServiceGateWayBindingStub tbs = new TIPTOPServiceGateWayBindingStub(url,service);
			GetCHDaxmt620Request_GetCHDaxmt620Request parameters = new GetCHDaxmt620Request_GetCHDaxmt620Request();
			parameters.setRequest(parameter);
			GetCHDaxmt620Response_GetCHDaxmt620Response rsp = tbs.getCHDaxmt620(parameters);
			response=rsp.getResponse();
			System.out.println("result"+response.toString());
			Xmlparse t=new Xmlparse();
			Map xmlMap=t.parseXml2Map(response.toString());
			rstInfo.put("result", xmlMap.get("result"));
			rstInfo.put("returnInfo", xmlMap.get("returnInfo"));
			rstInfo.put("returnCode", xmlMap.get("returnCode"));
		} catch (Exception e) {
			rstInfo.put("result", false);
			rstInfo.put("returnInfo", e.toString());
			rstInfo.put("returnCode", "");
		}
		return rstInfo;
	}
	//开票接口专用
	public JSONObject sychkaipInfo(String endpoint , String parameter) {
		JSONObject rstInfo = new JSONObject();
		String response="";
		try {
			Service service = new Service();
			URL url = new URL(endpoint);
			TIPTOPServiceGateWayBindingStub tbs = new TIPTOPServiceGateWayBindingStub(url,service);
			GetXSKPaxmt670Request_GetXSKPaxmt670Request parameters = new GetXSKPaxmt670Request_GetXSKPaxmt670Request();
			parameters.setRequest(parameter);
			GetXSKPaxmt670Response_GetXSKPaxmt670Response rsp = tbs.getXSKPaxmt670(parameters);
			response=rsp.getResponse();
			System.out.println("result"+response.toString());
			Xmlparse t=new Xmlparse();
			Map xmlMap=t.parseXml2Map(response.toString());
			rstInfo.put("result", xmlMap.get("result"));
			rstInfo.put("returnInfo", xmlMap.get("returnInfo"));
			rstInfo.put("returnCode", xmlMap.get("returnCode"));
		} catch (Exception e) {
			rstInfo.put("result", false);
			rstInfo.put("returnInfo", e.toString());
			rstInfo.put("returnCode", "");
		}
		return rstInfo;
	}
}
