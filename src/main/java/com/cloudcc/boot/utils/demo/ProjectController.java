package com.cloudcc.boot.utils.demo;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.boot.utils.util.CommonUtil;
import com.cloudcc.client.CCObject;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
@Slf4j
public class ProjectController {

	/**
	 * 更新业务机会的流程处理状态
	 * @author caoyx
	 * @param projectno 项目编号
	 * @param processstatus 处理状态
	 * @return {"SuccessFlag":"0/1","ErrorMessage":"","ReturnData":""}
	 */
	public JSONObject updateProject (String projectno,String processstatus) {
		JSONObject returnInfo = new JSONObject();
		log.info(projectno+":"+processstatus);
		
		try {
			JSONObject status = JSONObject.fromObject(processstatus);
		} catch (Exception e) {
			returnInfo.put("SuccessFlag", "0");
			returnInfo.put("ErrorMessage", processstatus+"格式错误");
			returnInfo.put("ReturnData", "[]");
			return returnInfo;
		}
		
		try {
			JSONObject status = JSONObject.fromObject(processstatus);
			ResourceBundle prb = PropertyResourceBundle.getBundle("conf");// 获取配置文件
			String userName = prb.getString("userName");
			String pwd = prb.getString("password");
			UserInfo uInfo = new UserInfo(userName, pwd);
			CCService cs = new CCService(uInfo);
			List<CCObject> olist = cs.cquery("Opportunity","projectno='"+projectno+"'");
			if (olist.size() > 0) {
				for (CCObject co : olist) {
					co.put("processstatus", status.get("quotationstatus"));
					co.put("contractstatus", status.get("contractstatus"));
					co.put("paymentstatus", status.get("paymentstatus"));
				} 
				cs.update(olist);
				returnInfo.put("SuccessFlag", "1");
				returnInfo.put("ErrorMessage", "");
				returnInfo.put("ReturnData", "[]");
			} else {
				returnInfo.put("SuccessFlag", "0");
				returnInfo.put("ErrorMessage", "项目编号"+projectno+"不存在");
				returnInfo.put("ReturnData", "[]");
			}
		} catch (Exception ex) {
			returnInfo.put("SuccessFlag", "0");
			returnInfo.put("ErrorMessage", ""+ex.getMessage());
			returnInfo.put("ReturnData", "[]");
		}
		return returnInfo;
	}
	
	/**
	 * 根据项目编号获取项目信息
	 * @param projectno 项目编号
	 * @return JSON 
	 */
	public JSONObject getProjectInfo (String projectno) {
		JSONObject returnInfo = new JSONObject();
		try {
			CommonUtil cu = new CommonUtil();
			log.info("Request:ProjectController.getProjectInfo("+projectno+")");
			ResourceBundle prb = PropertyResourceBundle.getBundle("conf");// 获取配置文件
			String userName = prb.getString("userName");
			String pwd = prb.getString("password");
			UserInfo uInfo = new UserInfo(userName, pwd);
			CCService cs = new CCService(uInfo);
			String sql = "select date_format(t0.pipelinegenerateddate,'%Y/%m/%d') pipelinegenerateddate,t0.name,t0.id,t1.acountnum,t2.employeenum salesmancode,t2.company,t2.teamname from Opportunity t0,account t1,ccuser t2" ;
			String condition = " where t1.id = t0.khmc and t2.id = t0.ownerid and projectno='"+projectno+"'";
			List<CCObject> olist = cs.cqlQuery("Opportunity",sql + condition );
			if (olist.size() > 0) {
				JSONObject oppt = new JSONObject();
				oppt.put("CaseNo", projectno);
				oppt.put("CaseName",cu.nvlEncode(olist.get(0).get("name")));
				oppt.put("CustomerId",cu.nvlEncode(olist.get(0).get("acountnum")));
				oppt.put("Salemancode",cu.nvlEncode(olist.get(0).get("salesmancode")));
				oppt.put("CaseDate",cu.nvlEncode(olist.get(0).get("pipelinegenerateddate")));
				oppt.put("SectionName",cu.nvlEncode(olist.get(0).get("teamname")));
				oppt.put("BranchName",cu.nvlEncode(olist.get(0).get("company")));
				returnInfo.put("SuccessFlag", "1");
				returnInfo.put("ErrorMessage", "");
				returnInfo.put("ReturnData", oppt.toString());
				log.info(returnInfo+"");
			} else {
				returnInfo.put("SuccessFlag", "0");
				returnInfo.put("ErrorMessage", "项目编号"+projectno+"不存在");
				returnInfo.put("ReturnData", "[]");
				log.error(returnInfo+"");
			}
		} catch (Exception ex) {
			returnInfo.put("SuccessFlag", "0");
			returnInfo.put("ErrorMessage", "查询失败："+ex.getMessage());
			returnInfo.put("ReturnData", "[]");
			log.error(returnInfo+"");
		}
		return returnInfo;
	}
	
	public static void main(String[] args) {
		ProjectController pc = new ProjectController();
		pc.getProjectInfo("900201811220000");
	}
}
