package com.cloudcc.boot.utils.demo;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.boot.utils.util.ServiceResult;
import com.cloudcc.client.CCObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SpHandle {
    private CCService cs ;
    private UserInfo userInfo;
    public SpHandle(UserInfo userInfo){
        cs = new CCService(userInfo);
        this.userInfo = userInfo;
    }
    public String Spin(String recid,String productman,String ownerid,String ejzls) {
        String fhxx = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String date = df.format(new Date());//当前时间
        String empcode = "";
        List<CCObject> list1 = cs.cquery("zzyhlb","id='"+productman+"'");
        if(list1.size()>0){
            empcode = list1.get(0).get("loginname")==null?"":String.valueOf(list1.get(0).get("loginname"));
        }
        String empcode2 = "";
        List<CCObject> list2 = cs.cquery("zzyhlb","id='"+ejzls+"'");
        if(list2.size()>0){
            empcode2 = list2.get(0).get("loginname")==null?"":String.valueOf(list2.get(0).get("loginname"));
        }
        String loginname = "";
        List<CCObject> userlist = cs.cquery("ccuser","id='"+ownerid+"'");
        if(userlist.size()>0){
            loginname = userlist.get(0).get("loginname")==null?"":String.valueOf(userlist.get(0).get("loginname"));
            loginname=loginname.substring(0,loginname.indexOf("@"));
        }
        try{
            CCObject his = new CCObject("custprocesshis");
            his.put("recordid", recid);
            his.put("operatingtime", date);
            his.put("stepststus", "已提交");
            his.put("stepname", "已经提交的批准请求");
            his.put("remark", "");
            his.put("ownername", ownerid);
            his.put("empcode", loginname);
            ServiceResult rs = cs.insert(his);
            String id = rs.get("id")==null?"":rs.get("id").toString();
            if(!"".equals(productman)){
                CCObject his1 = new CCObject("custprocesshis");
                his1.put("recordid", recid);
                his1.put("operatingtime", date);
                his1.put("stepststus", "审批中");
                his1.put("stepname", "负责人审批");
                his1.put("remark", "");
                his1.put("actorid", productman);
                his1.put("empcode", empcode);
                cs.insert(his1);
            }
            if(!"".equals(ejzls)){
                CCObject his1 = new CCObject("custprocesshis");
                his1.put("recordid", recid);
                his1.put("operatingtime", date);
                his1.put("stepststus", "审批中");
                his1.put("stepname", "负责人审批");
                his1.put("remark", "");
                his1.put("actorid", ejzls);
                his1.put("empcode", empcode2);
                cs.insert(his1);
            }

            List<CCObject> qualitylist = cs.cquery("qualityInfoColl","id='"+recid+"'");
            if(qualitylist.size()>0){
                qualitylist.get(0).put("appstatus","审批中");
                cs.update(qualitylist.get(0));
            }
            fhxx = "保存成功";
        }catch(Exception x){
            fhxx = x.getMessage();
        }
        return fhxx;
    }
    public String approv(String recid,String approvalstatus,String splsid,String remark,String jhwcsj,String gzlx,String gzjlyc,String clcs,String gzjlec,String sjwcsj,String yyfx,String yhfkd) {
        String fhxx = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String date = df.format(new Date());//当前时间
        try{
            String fzrname = "";
            String ejzls = "";
            String owename = "";
            String empcode = "";
            String empcode1 = "";
            String loginname = "";
            List<CCObject> list = cs.cquery("qualityInfoColl","id='"+recid+"'");
            if(list.size()>0){
                fzrname = list.get(0).get("productman")==null?"":String.valueOf(list.get(0).get("productman"));
                ejzls = list.get(0).get("ejzls")==null?"":String.valueOf(list.get(0).get("ejzls"));
                owename = list.get(0).get("ownerid")==null?"":String.valueOf(list.get(0).get("ownerid"));
                if(!"".equals(fzrname)){
                    List<CCObject> list1 = cs.cquery("zzyhlb","id='"+fzrname+"'");
                    if(list1.size()>0){
                        empcode = list1.get(0).get("loginname")==null?"":String.valueOf(list1.get(0).get("loginname"));
                    }
                }
                if(!"".equals(ejzls)){
                    List<CCObject> list2 = cs.cquery("zzyhlb","id='"+ejzls+"'");
                    if(list2.size()>0){
                        empcode1 = list2.get(0).get("loginname")==null?"":String.valueOf(list2.get(0).get("loginname"));
                    }
                }
                List<CCObject> userlist = cs.cquery("cccuser","id='"+owename+"'");
                if(userlist.size()>0){
                    loginname = userlist.get(0).get("loginname")==null?"":String.valueOf(userlist.get(0).get("loginname"));
                    loginname=loginname.substring(0,loginname.indexOf("@"));
                }
            }
            List<CCObject> hislist = cs.cquery("custprocesshis","id='"+splsid+"'");
            if(hislist.size()>0){
                String stepname = hislist.get(0).get("stepname")==null?"":String.valueOf(hislist.get(0).get("stepname"));
                if(stepname.equals("负责人审批")){
                    hislist.get(0).put("stepstatus",approvalstatus);
                    hislist.get(0).put("remark",remark);
                    hislist.get(0).put("operatingtime",date);
                    cs.update(hislist.get(0));
                    List<CCObject> hislist1 = cs.cquery("custprocesshis","stepststus='审批中' and is_deleted='0'");
                    if(hislist1.size()>0){
                        hislist1.get(0).put("stepstatus",approvalstatus);
                        hislist1.get(0).put("remark",remark);
                        hislist1.get(0).put("operatingtime",date);
                        cs.update(hislist1.get(0));
                    }
                    CCObject his1 = new CCObject("custprocesshis");
                    his1.put("recordid", recid);
                    his1.put("operatingtime", date);
                    his1.put("stepstatus", "审批中");
                    his1.put("stepname", "提交人审批");
                    his1.put("remark", "");
                    his1.put("ownername", owename);
                    his1.put("empcode", loginname);
                    cs.insert(his1);
                    list.get(0).put("plansdate",jhwcsj);
                    list.get(0).put("faulttypes",gzlx);
                    list.get(0).put("failuremechanism",gzjlyc);
                    list.get(0).put("measures",clcs);
                    list.get(0).put("failuremechanismtwo",gzjlec);
                    list.get(0).put("actualcompletiontime",sjwcsj);
                    list.get(0).put("analysis",yyfx);
                    list.get(0).put("userlist",yhfkd);
                    cs.update(list.get(0));
                }else if(stepname.equals("提交人审批")){
                    if(approvalstatus.equals("审批通过")){
                        hislist.get(0).put("stepstatus",approvalstatus);
                        hislist.get(0).put("remark",remark);
                        hislist.get(0).put("operatingtime",date);
                        cs.update(hislist.get(0));
                        list.get(0).put("appstatus",approvalstatus);
                        cs.update(list.get(0));
                    }else if(approvalstatus.equals("审批拒绝")){
                        hislist.get(0).put("stepstatus",approvalstatus);
                        hislist.get(0).put("remark",remark);
                        hislist.get(0).put("operatingtime",date);
                        cs.update(hislist.get(0));
                        if(!"".equals(fzrname)){
                            CCObject his1 = new CCObject("custprocesshis");
                            his1.put("recordid", recid);
                            his1.put("operatingtime", date);
                            his1.put("stepstatus", "审批中");
                            his1.put("stepname", "负责人审批");
                            his1.put("remark", "");
                            his1.put("actorid", fzrname);
                            his1.put("empcode",empcode);
                            cs.insert(his1);
                        }
                        if(!"".equals(ejzls)){
                            CCObject his1 = new CCObject("custprocesshis");
                            his1.put("recordid", recid);
                            his1.put("operatingtime", date);
                            his1.put("stepstatus", "审批中");
                            his1.put("stepname", "负责人审批");
                            his1.put("remark", "");
                            his1.put("actorid", ejzls);
                            his1.put("empcode",empcode1);
                            cs.insert(his1);
                        }
                    }
                }
            }
            fhxx = "保存成功";
        }catch(Exception x){
            fhxx = x.getMessage();
        }
        return fhxx;
    }
    public String addapprov(String splsid,String addapprover) {
        String fhxx = "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String date = df.format(new Date());//当前时间
        try{
            String empcode = "";
            List<CCObject> list1 = cs.cquery("zzyhlb","id='"+addapprover+"'");
            if(list1.size()>0){
                empcode = list1.get(0).get("loginname")==null?"":String.valueOf(list1.get(0).get("loginname"));
            }
            List<CCObject> hislist = cs.cquery("custprocesshis","id='"+splsid+"'");
            if(hislist.size()>0){
                String recordid = hislist.get(0).get("recordid")==null?"":String.valueOf(hislist.get(0).get("recordid"));
                hislist.get(0).put("actorid",addapprover);
                hislist.get(0).put("empcode",empcode);
                hislist.get(0).put("operatingtime",date);
                cs.update(hislist.get(0));
                List<CCObject> list = cs.cquery("qualityInfoColl","id='"+recordid+"'");
                if(list.size()>0){
                    list.get(0).put("productman",addapprover);
                    cs.update(list.get(0));
                }
            }
            fhxx = "保存成功";
        }catch(Exception x){
            fhxx = x.getMessage();
        }
        return fhxx;
    }
}