package com.cloudcc.boot.utils.oa;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.client.CCObject;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: LGH
 * @Date: 2020/4/13 0013 15:24
 */
@Slf4j
public class ApprovalToOAYbClass{
    public String sptsoa(String recid, String sprname, UserInfo userInfo) throws Exception{
        CCService cs = new CCService(userInfo);
        net.sf.json.JSONObject json = new net.sf.json.JSONObject();//参数
        net.sf.json.JSONObject sendJson = new net.sf.json.JSONObject();
        String returninfo = "";//返回信息
        String objapi = recid.substring(0,3);
        String schemetable_name = "";
        String label = "";
        List<CCObject> objList = cs.cqlQuery("quote","select schemetable_name,label from tp_sys_object where prefix='"+objapi+"'");
        if(objList.size() > 0){
            schemetable_name = objList.get(0).get("schemetable_name")==null?"":String.valueOf(objList.get(0).get("schemetable_name"));//对象名
            label = objList.get(0).get("label")==null?"":String.valueOf(objList.get(0).get("label"));//对象标签
        }
        List<CCObject> bjdList = cs.cquery(schemetable_name,"id='"+recid+"'");//查询记录
        if(bjdList.size() > 0){
            String bjmc = bjdList.get(0).get("name")==null?"":String.valueOf(bjdList.get(0).get("name"));//名称
            String ownerid = bjdList.get(0).get("ownerid")==null?"":String.valueOf(bjdList.get(0).get("ownerid"));
            List<CCObject> userList = cs.cqlQuery("tp_sys_user","select * from tp_sys_user where id='"+ownerid+"'");
            String sqrygbh = userList.get(0).get("employee_num")==null?"":String.valueOf(userList.get(0).get("employee_num"));
            String owneridccname = userList.get(0).get("name")==null?"":String.valueOf(userList.get(0).get("name"));
            Date date = new Date();
            SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String time = dateFormat.format(date);
            json.put("syscode", "CRM");//CRM
            json.put("flowid", recid);//记录id
            json.put("requestname", label+"："+bjmc+" 等待您的审批"+"(申请人:"+owneridccname+")");//项目名称等待您的审批
            json.put("workflowname", label+"审批");//费用申请/项目立项/报价单审批
            json.put("nodename", label+"审批");//费用申请/项目立项/报价单审批
            json.put("receiver", sprname);//审批人  sprname
            OaSendYb ss = new OaSendYb(userInfo);
            log.info("已办推送日志"+json);

            String result = ss.decode_new(json);
            returninfo = result;
        }
        return returninfo;
        //return json.toString()+"";
    }

    public static String getURLEncoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String URLDecoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLDecoder.decode(str, "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
