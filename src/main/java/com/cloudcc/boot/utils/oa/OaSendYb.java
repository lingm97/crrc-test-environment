package com.cloudcc.boot.utils.oa;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import net.sf.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @Author: LGH
 * @Date: 2020/4/13 0013 15:26
 */
public class OaSendYb{
    private CCService cs ;
    private UserInfo userInfo;
    public OaSendYb(UserInfo userInfo){
        cs = new CCService(userInfo);
        this.userInfo = userInfo;
    }

    public String decode_new(JSONObject json) {
        String result = null;
        try {
            // 创建url资源
            URL url = new URL("http://120.224.2.6:8082/rest/ofs/ProcessDoneRequestByJson");
            // 建立http连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置允许输出
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 设置不用缓存
            conn.setUseCaches(false);
            // 设置传递方式
            conn.setRequestMethod("POST");
            // 设置维持长连接
            conn.setRequestProperty("Connection", "Keep-Alive");
            // 设置文件字符集:
            conn.setRequestProperty("Charset", "UTF-8");
            //转换为字节数组
            byte[] data = (json.toString()).getBytes();
            // 设置文件长度
            conn.setRequestProperty("Content-Length", String.valueOf(data.length));
            // 设置文件类型:
            conn.setRequestProperty("Content-Type","application/json");
            // 开始连接请求
            conn.connect();
            OutputStream out = conn.getOutputStream();
            // 写入请求的字符串
            out.write((json.toString()).getBytes());
            out.flush();
            out.close();


            // 请求返回的状态
            if (conn.getResponseCode() == 200) {
                // 请求返回的数据
                InputStream in = conn.getInputStream();
                try {
                    byte[] data1 = new byte[in.available()];
                    in.read(data1);
                    // 转成字符串
                    result = new String(data1);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            } else {
                result = "连接失败";
            }
        } catch (Exception e) {

        }
        return result;
    }
}