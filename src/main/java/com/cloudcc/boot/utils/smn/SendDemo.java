package com.cloudcc.boot.utils.smn;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
/**
 * @Author: LGH
 * @Date: 2020/1/16 0016 14:14
 *     /*
 *     pom.xml
 *     <dependency>
 *       <groupId>com.aliyun</groupId>
 *       <artifactId>aliyun-java-sdk-core</artifactId>
 *       <version>4.0.3</version>
 *     </dependency>
 *
 */
public class SendDemo {

        public static void main(String[] args) {
            DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI4FgrZQowSbDQrtqJhqyC", "x86AfKrK1GFlIJ9w6TU8ObSRmOgyRq");
            IAcsClient client = new DefaultAcsClient(profile);

            CommonRequest request = new CommonRequest();
            request.setMethod(MethodType.POST);
            request.setDomain("dysmsapi.aliyuncs.com");
            request.setVersion("2017-05-25");
            request.setAction("SendSms");
            request.putQueryParameter("RegionId", "cn-hangzhou");
            //电话号码
            request.putQueryParameter("PhoneNumbers", "15269156980");
            //签名
            request.putQueryParameter("SignName", "泰开售后服务");
            //模版id
            request.putQueryParameter("TemplateCode", "SMS_182671505");
            //模版值
            request.putQueryParameter("TemplateParam", "{\"code\":\"abcd\"}");
            //上行吗
            request.putQueryParameter("SmsUpExtendCode", "111111");
            //业务扩展码
            request.putQueryParameter("OutId", "a05202088F5116AdWYHt");

            try {
                CommonResponse response = client.getCommonResponse(request);
                System.out.println(response.getData());
            } catch (ServerException e) {
                e.printStackTrace();
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }

}

