package com.cloudcc.boot.utils.test;

import com.cloudcc.boot.utils.util.HttpRequest;

import java.io.UnsupportedEncodingException;

public class HttpTest {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String httpAddr = "http://114.116.231.93/web/ws/r/aws_ttsrv2";
		//方法
		String xml="  <Request>\n" +
				"<Access>\n" +
				"<Authentication user=\"tiptop\" password=\"tiptop\" />\n" +
				"<Connection application=\"BC\" source='114.116.231.93' />\n" +
				"<Organization name='DS1' />\n" +
				"<Locale language=\"zh_cn\" />\n" +
				"</Access>\n" +
				"<RequestContent>\n" +
				"<Document>\n" +
				"<RecordSet id=\"1\">\n" +
				"<Master name=\"occ_file\">\n" +
				"<Record>\n" +
				"<Field name=\"type\" value=\"A\"/>\n" +
				"<Field name=\"occ01\" value=\"20190809000\"/>\n" +
				"<Field name=\"occ18\" value=\"上海理光数码设备有限公司\"/>\n" +
				"<Field name=\"occ03\" value=\"终端客户\"/>\n" +
				"<Field name=\"occ04\" value=\"0052019734B5392TpaYD\"/>\n" +
				"<Field name=\"occ05\" value=\"永久性\"/>\n" +
				"<Field name=\"occ63\" value=\"12\"/>\n" +
				"<Field name=\"occ41\" value=\"X13\"/>\n" +
				"<Field name=\"occ42\" value=\"CNY\"/>\n" +
				"<Field name=\"occ22\" value=\"东部大区\"/>\n" +
				"<Field name=\"occ21\" value=\"中国\"/>\n" +
				"<Field name=\"occ1009\" value=\"北京市\"/>\n" +
				"<Field name=\"occ1010\" value=\"北京市\"/>\n" +
				"<Field name=\"occ1011\" value=\"2\"/>\n" +
				"<Field name=\"occ1012\" value=\"1\"/>\n" +
				"<Field name=\"occ11\" value=\"123\"/>\n" +
				"<Field name=\"occ930\" value=\"电力电子\"/>\n" +
				"</Record>\n" +
				"</Master>\n" +
				"</RecordSet>\n" +
				"</Document>\n" +
				"</RequestContent>\n" +
				"</Request>";

		String str = HttpRequest.sendPost(httpAddr, xml);
		

		System.out.println(new String(str.getBytes("gbk"),"utf-8"));
	}

}
