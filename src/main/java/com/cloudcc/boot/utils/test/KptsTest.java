package com.cloudcc.boot.utils.test;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.boot.utils.util.DesEncoder;
import com.cloudcc.boot.utils.util.HttpClientUtil;
import com.cloudcc.client.CCObject;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Slf4j
public class KptsTest {

	public static void main(String[] args) throws Exception {
		DesEncoder de = DesEncoder.getInstance();
		UserInfo uInfo = new UserInfo("shirc@126.com", de.setDesString("I7n1btQRjhMkbZNuYvCaEw=="));
		CCService cs = new CCService(uInfo);
		log.info("=====>>>>>开始轮循开票申请  {}",System.currentTimeMillis());
            List<CCObject> klist = cs.cquery("kpsq"," beizhu = '发票OA已开，不需要重新开票，crm补录开票流程(2020-06-18 导入系统)'  and is_Deleted='0'");
            if (klist.size() > 0) {
                for (CCObject coInfo : klist) {
                    Map<String, String> param = new HashMap<String, String>();
                    param.put("kpsqid", coInfo.get("id").toString());
                    String url = "http://114.116.239.15:8080/api/kpsq/push";
                    String str = HttpClientUtil.doPost(url, param);
					log.info(str);
                }
            }
            log.info("=====>>>>>结束轮循开票申请  {}",System.currentTimeMillis());



	}
}
