package com.cloudcc.boot.utils.test;

import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.boot.utils.util.ServiceResult;
import com.cloudcc.client.CCObject;
import net.sf.json.JSONObject;

import java.util.List;

/**
 * author : 林贵美 on 2019-12-18
 * 目的：产品处理类
 **/
public class ProductClass {
    private CCService cs ;
    private UserInfo userInfo;
    public ProductClass(UserInfo userInfo){
        cs = new CCService(userInfo);
        this.userInfo = userInfo;
    }

    /**
     * 林贵美 2019-12-18创建
     * 方法名：createProduct
     * 目的：合同评审，评审意见通过后，生成产品数据
     **/
    public JSONObject createProduct (String category, String productmodel, String majordept) throws Exception {
        JSONObject returnInfo = new JSONObject();
        String sort="";
        int n=0;
        try{
            CCObject newProduct = new CCObject("product");//产品
            newProduct.put("PartType", category);//产品的自制件类型
            newProduct.put("name", productmodel);//产品型号
            newProduct.put("recordtype", "201901F19C955FDUAKb8");//记录类型
            List<CCObject> cpList  = cs.cqlQuery("product","select max(to_number(sortnum)) as zsl from product where is_deleted='0'");//产品
            sort=cpList.get(0).get("zsl").toString();
            n=Integer.parseInt(sort)+1;
            newProduct.put("sortnum",String.valueOf(n));//排序序号
            if (!"".equals(majordept)) {
                String arr[]=majordept.split(";");
                String developmentdept = "";
                for(int i=0;i<arr.length;i++){
                    developmentdept = arr[0];
                }
                List<CCObject> departmentList = cs.cquery("department", "id='"+developmentdept+"'");//部门
                String departmentName = "";
                if (departmentList.size() > 0) {
                    departmentName = departmentList.get(0).get("name").toString();//部门名称
                }
                newProduct.put("developmentdept", departmentName);//研制部门
            }

            ServiceResult rs = cs.insert(newProduct);//插入记录
            String cpid = rs.get("id")==null?"":rs.get("id").toString();
            returnInfo.put("success", "S");
            returnInfo.put("message",cpid);
        }catch(Exception ex){
            returnInfo.put("success", "E");
            returnInfo.put("message","生成产品发生错误，请联系管理员！");
        }
        return	returnInfo;
    }
}