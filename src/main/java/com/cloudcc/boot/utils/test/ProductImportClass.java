package com.cloudcc.boot.utils.test;

/**
 * @Author: LGH
 * @Date: 2021/1/26 0026 16:52
 */


import com.cloudcc.boot.model.UserInfo;
import com.cloudcc.boot.utils.util.CCService;
import com.cloudcc.client.CCObject;
import net.sf.json.JSONObject;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * author : 林贵美 on 2020-03-26
 **/
public class ProductImportClass {
    private UserInfo userInfo;
    public ProductImportClass(UserInfo userInfo){
        this.userInfo = userInfo;
    }
    /**
     * 林贵美 2020-03-26创建
     **/
    public JSONObject exec(List<CCObject> list, String HandleType) throws InterruptedException {
        JSONObject returnInfo = new JSONObject();
        try{
            int count = 500;                   //一个线程处理300条数据
            int listSize = list.size();        //数据集合大小
            int runSize = (listSize/count)+1;  //开启的线程数
            List<CCObject> newlist = null;       //存放每个线程的执行数据
            ExecutorService executor = Executors.newFixedThreadPool(runSize);      //创建一个线程池，数量和开启线程的数量一样
            //创建两个个计数器
            CountDownLatch begin = new CountDownLatch(1);
            CountDownLatch end = new CountDownLatch(runSize);

            //循环创建线程
            for (int i = 0; i < runSize; i++) {
                //计算每个线程执行的数据
                if ((i + 1) == runSize) {
                    int startIndex = (i * count);
                    int endIndex = list.size();
                    newlist = list.subList(startIndex, endIndex);
                } else {
                    int startIndex = (i * count);
                    int endIndex = (i + 1) * count;
                    newlist = list.subList(startIndex, endIndex);
                }
                if ("insert".equals(HandleType)) {
                    //线程类
                    MyThreadInsert mytheadInsert = new MyThreadInsert(newlist, begin, end, userInfo);
                    //这里执行线程的方式是调用线程池里的executor.execute(mythead)方法。
                    System.out.println("创建了"+i+"个线程");
                    executor.execute(mytheadInsert);
                } else {
                    //线程类
                    MyThread mythead = new MyThread(newlist, begin, end, userInfo);
                    //这里执行线程的方式是调用线程池里的executor.execute(mythead)方法。
                    System.out.println("创建了"+i+"个线程");
                    executor.execute(mythead);
                }
            }

            begin.countDown();
            end.await();

            //执行完关闭线程池
            executor.shutdown();
            returnInfo.put("success", "S");
            returnInfo.put("message", "true"+HandleType);
        }catch(Exception ex){
            returnInfo.put("success", "E");
            returnInfo.put("message","");
        }
        return	returnInfo;
    }
}
class MyThread implements Runnable {
    private List<CCObject> list;
    private CountDownLatch begin;
    private CountDownLatch end;
    private UserInfo userInfo;
    private CCService cs ;

    //创建个构造函数初始化 list,和其他用到的参数
    public MyThread(List<CCObject> list, CountDownLatch begin, CountDownLatch end, UserInfo userInfo) {
        this.list = list;
        this.begin = begin;
        this.end = end;
        this.userInfo=userInfo;
        cs = new CCService(userInfo);
    }

    @Override
    public void run() {
        try{
            //执行完让线程直接进入等待
            begin.await();
            //for (CCObject ccproduct : list) {
            //分批 批量插入
            cs.update(list);
            //}


        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            //这里要主要了，当一个线程执行完 了计数要减一不然这个线程会被一直挂起
            // ，end.countDown()，这个方法就是直接把计数器减一的
            end.countDown();
        }
    }
}
class MyThreadInsert implements Runnable {
    private List<CCObject> list;
    private CountDownLatch begin;
    private CountDownLatch end;
    private UserInfo userInfo;
    private CCService cs ;

    //创建个构造函数初始化 list,和其他用到的参数
    public MyThreadInsert(List<CCObject> list, CountDownLatch begin, CountDownLatch end, UserInfo userInfo) {
        this.list = list;
        this.begin = begin;
        this.end = end;
        this.userInfo=userInfo;
        cs = new CCService(userInfo);
    }

    @Override
    public void run() {
        try{
            //执行完让线程直接进入等待
            begin.await();
            //for (CCObject ccproduct : list) {
            //分批 批量插入
            cs.insert(list);
            //}


        }  catch (Exception e) {
            e.printStackTrace();
        } finally {
            //这里要主要了，当一个线程执行完 了计数要减一不然这个线程会被一直挂起
            // ，end.countDown()，这个方法就是直接把计数器减一的
            end.countDown();
        }
    }
}