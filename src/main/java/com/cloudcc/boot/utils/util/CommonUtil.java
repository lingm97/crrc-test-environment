package com.cloudcc.boot.utils.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtil {
	public String nvlEncode (Object val) {
		if (val == null) {
			return "";
		} else {
			return String.valueOf(val);
		}
	}
	/**
	 * 格式化cloudcc日期
	 * @param time
	 * @return
	 */
	public String timeTransfer (Object time) {
		if (time == null || "".equals(time)) {
			return "";
		} else {
			time = time +".000";
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			try {
				Date datetime = sdf1.parse((String)time);
				return sdf1.format(datetime);
			} catch (ParseException e) {
				return "";
			}
		}
	}
	public String simpleFormat (Object time) {
		if (time == null || "".equals(time)) {
			return "";
		} else {
			SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
			try {
				String timestr = time+"";
				Date tmpval = null;
				if (timestr.indexOf(":") != -1) {
					tmpval = sdft.parse(timestr);
				} else {
					tmpval = sdfd.parse(timestr);
				}
				return sdf1.format(tmpval);
			} catch (ParseException e) {
				return "";
			}
		}
	}
	
	public static String simpleFormat (Object time,String format) {
		if (time == null || "".equals(time)) {
			return "";
		} else {
			SimpleDateFormat sdft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf1 = new SimpleDateFormat(format);
			try {
				String timestr = time+"";
				Date tmpval = null;
				if (timestr.indexOf(":") != -1) {
					tmpval = sdft.parse(timestr);
				} else {
					tmpval = sdfd.parse(timestr);
				}
				return sdf1.format(tmpval);
			} catch (ParseException e) {
				return "";
			}
		}
	}
	
	
}
