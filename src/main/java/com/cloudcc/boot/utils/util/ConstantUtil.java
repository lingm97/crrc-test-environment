package com.cloudcc.boot.utils.util;

public class ConstantUtil {
	public static String Quotation_ORDERTYPE_CODE = "28";//订单类型
	public static String Quotation_QUATATIONTYPE_CODE = "34";//报价类型
	public static String Quotation_DISCOUNTTYPE_CODE = "03";//特价类型
	public static String Quotation_APPLICATIONTYPE_CODE = "07";//申请类型
	public static String Quotation_MCFLAG_CODE = "101";//异地委托统一结算
	public static String Quotation_DELEGATE_CODE = "46";//异地委托选项
	public static String Quotation_FREECOPY_CODE = "102";//是否包含免费印张
	public static String Quotation_ITEMGROUP_CODE = "23";//货品类型
	public static String Quotation_BILLINGCYCLE_CODE = "04";//结算周期
	public static String DiscountApply_DISCOUNTITEMS_CODE = "09";//特价对象
	public static String DiscountApply_SALESTYPE_CODE = "47";//销售方式
	public static String DiscountApply_METERTYPE_CODE = "103";//计费项目

	public static String MCINFO_CHARGETTYPE_CODE="104";
	public static String VERIFY_CODE = "cloudcccrm";
}
