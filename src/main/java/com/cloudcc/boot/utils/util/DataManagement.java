package com.cloudcc.boot.utils.util;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.Callable;
@Slf4j
public class DataManagement implements Callable<ServiceResult> {
	private ServiceResult sr;
	private CCService cs; 
	@Override
	public ServiceResult call() {
		if ("insertList".equals(this.method)) {
			log.info(Thread.currentThread().getName()+" start...");
			//sr=cs.batchInsert(this.objectApiName, this.datalist);
			try {
				sr=cs.insertList(objectApiName, datalist);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} if ("batchInsert".equals(this.method)) {
			log.info(Thread.currentThread().getName()+" start...");
			//sr=cs.batchInsert(this.objectApiName, this.datalist);
			sr=cs.batchInsert(objectApiName, datalist);
		} else if ("updateList".equals(this.method)) {
			try {
				log.info(Thread.currentThread().getName()+" start...");
				sr=cs.updateList(this.objectApiName, this.datalist);
			} catch (UnsupportedEncodingException e) {
				log.error(e.getMessage());
				e.printStackTrace();
			}
		}
		return sr;
	}
	
	List<JSONObject> datalist;
	String objectApiName;
	String method;
	public DataManagement(String method,String object,List<JSONObject> datalist,CCService ccs) {
		this.method = method;
		this.objectApiName = object;
		this.datalist = datalist;
		this.cs = ccs;
	}
	//SR Getter Method
	public ServiceResult getSr() {
		return this.sr;
	}
	
	//SR Setter Method
	public void setSr(ServiceResult sr) {
		this.sr = sr;
	}
}
