package com.cloudcc.boot.utils.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class DesEncoder {
	static DesEncoder instance;
    static Key key;
    static ResourceBundle prb = PropertyResourceBundle.getBundle("conf");// 获取配置文件
    static Cipher encryptCipher;
    static Cipher decryptCipher;
    static String maskcode = prb.getString("maskcode");// 获取秘钥
    public DesEncoder() {
    }

    protected DesEncoder(String strKey) {
        key = setKey(strKey);
        try {
            encryptCipher = Cipher.getInstance("AES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, key);
            decryptCipher = Cipher.getInstance("AES");
            decryptCipher.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
 
    }
 
    public static  DesEncoder getInstance() {
        if (instance == null) {
            instance = new DesEncoder(maskcode);
        }

        return instance;
    }
 
    //  根据参数生成KEY
    private Key setKey(String strKey) {
        try {
            KeyGenerator _generator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(strKey.getBytes());
            _generator.init(256, secureRandom);
            return _generator.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return null;
    }
 
    //  加密String明文输入,String密文输出
    public String setEncString(String strMing) {
        BASE64Encoder base64en = new BASE64Encoder();
        try {
            byte[] byteMing = strMing.getBytes("UTF-8");
            byte[] byteMi = this.getEncCode(byteMing);
            return base64en.encode(byteMi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
    //加密以byte[]明文输入,byte[]密文输出
    private byte[] getEncCode(byte[] byteS) {
        byte[] byteFina = null;
        try {
            byteFina = encryptCipher.doFinal(byteS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteFina;
    }
 
    //   解密:以String密文输入,String明文输出
    public String setDesString(String strMi) {
        BASE64Decoder base64De = new BASE64Decoder();
        try {
            byte[] byteMi = base64De.decodeBuffer(strMi);
            byte[] byteMing = this.getDesCode(byteMi);
            return new String(byteMing, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
 
    // 解密以byte[]密文输入,以byte[]明文输出
    private byte[] getDesCode(byte[] byteD) {
        byte[] byteFina = null;
        try {
            byteFina = decryptCipher.doFinal(byteD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteFina;
    }
    public static void main(String[] args) {
		DesEncoder de = DesEncoder.getInstance();
		String pwd2 = de.setDesString("6kdYq57z1wpFAPDtdYuNA5o1y1f6n93l5F+fIK99Ma4=");
		String pwd=de.setEncString("TotalEnergies@2023");
		System.out.println(pwd);
		System.out.println(pwd2);
		String info ="[{\"amount\":19299.0,\"bigRock\":\"True\",\"connectedStatus\":false,\"connectivityServiceLetter\":\"-\",\"csa\":\"No\",\"cts\":null,\"customerActivity\":\"Active\",\"customerMachineUtilization\":\"High\",\"dealerCode\":\"J440\",\"dealerCustomerNumber\":\"4136793\",\"dealerSalesRep\":\"QIAO J PU\",\"dealerSalesRepNumber\":\"QJP\",\"description\":\"\\u6765\\u81ea\\u4e8eOLGA\\u5546\\u673a: \\n\\u91cd\\u8d1f\\u8377\\u52a8\\u81c2 (4180) (2020-02-03)\",\"descriptionEng\":\"OLGA Components with opportunities:\\nTRACK ROLLER (4180) (2020-02-03)\",\"division\":\"H\",\"drivetrainValue\":null,\"engineValue\":null,\"equipmentAge\":10.0,\"etApplication\":null,\"etSubApplication\":null,\"gasdIndustry\":\"CI: Retail\",\"hydraulicValue\":null,\"isoCurrencyCode\":\"CNY\",\"lastModifiedSmuDate\":\"2019-12-05\",\"leadStatus\":\"PENDING\",\"model\":\"320D\",\"onlinePurchaser\":\"No\",\"opportunityAmount\":null,\"opportunityStage\":null,\"prioritizedServiceEventId\":\"LSH_GASD_PSE_PRESIT_02222021_TEST697\",\"probabilityOfRepurchase\":\"0.4\",\"productGroup\":\"Machine\",\"productType\":\"MEDIUM EXCAVATORS (320-335)\",\"rebuildMethodology\":null,\"rejectionReason\":null,\"repairOption\":\"False\",\"serialNumber\":\"JFZ05704\",\"smu\":15579.0,\"smuUnits\":\"H\",\"sos\":\"No\",\"sourceCustomerName\":\"\\u7fdf\\u5fd7\\u519b\",\"subIndustry\":\"LARGE CONTRACTORS\",\"targetDate\":\"2021-02-07\",\"targetHours\":null,\"type\":\"Undercarriage\",\"utilizationRate\":184.0,\"utilizationType\":\"Actual\",\"yellowmarkOffering\":\"False\"}]";
	}
}