package com.cloudcc.boot.utils.util;

import java.io.UnsupportedEncodingException;

public class Http {

	public static void main(String[] args) throws UnsupportedEncodingException {
		String httpAddr = "http://www.webxml.com.cn/WebServices/TranslatorWebService.asmx";

		String jsonStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
				"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"  <soap:Body>\n" +
				"    <HelloWebXml xmlns=\"http://WebXml.com.cn/\" />\n" +
				"  </soap:Body>\n" +
				"</soap:Envelope>";
		
		String str = HttpRequest.sendPost(httpAddr, jsonStr);
		
		
		
		System.out.println(new String(str.getBytes("gbk"),"utf-8"));
	}

}
