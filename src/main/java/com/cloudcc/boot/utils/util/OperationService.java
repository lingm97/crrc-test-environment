/**
 * Author.
 * Comments:
 * Create Date
 * Modified By：
 * Version: 
 */
package com.cloudcc.boot.utils.util;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author lin
 *
 */
@Slf4j
public class OperationService {
	public static String operate(String url_str, String data_str) throws Exception {
		StringBuffer st = new StringBuffer("");
		String sc = "";

			// 获取接口应用连接
			URL url = new URL(url_str);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// 是否接受输出结果
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setConnectTimeout(900000000);
			con.setUseCaches(false);
			con.setReadTimeout(900000000);
			con.setRequestMethod("POST");
			
			OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
			out.write(data_str);
			out.close();

			// 使用输入流获取接口返回结果, 并返回给调用方
			InputStream in = con.getInputStream();
	
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			while ((sc = reader.readLine()) != null) {
				st.append(sc);
			}

			return st.toString();


	}
	public static String post(String url_str, String data_str,String token) throws Exception {
		StringBuffer st = new StringBuffer("");
		String sc = "";
		try {
			// 获取接口应用连接
			URL url = new URL(url_str);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			// 是否接受输出结果
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setConnectTimeout(20000); 
			con.setUseCaches(false);
			con.setReadTimeout(300000); 
			con.setRequestMethod("POST");
			// 数据提交方式为POST
			if(token != null){
				con.setRequestProperty("Authorization",token);
				con.setRequestProperty("Access-Control-Allow-Origin","*");
			}
			
			OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
			
			out.write(data_str);
			out.close();

			// 使用输入流获取接口返回结果, 并返回给调用方
			InputStream in = con.getInputStream();
	
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			while ((sc = reader.readLine()) != null) {
				st.append(sc);
			}

			return st.toString();
		} catch (Exception ex) {
			//logger.error(ex.getMessage());
			throw ex;
		}

	}
	public static String get(String url_str, String data_str,String token) throws Exception {
		StringBuffer st = new StringBuffer("");
		String sc = "";
		try {
			// 获取接口应用连接
			URL url = new URL(url_str);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// 是否接受输出结果
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setConnectTimeout(20000); 
			con.setUseCaches(false);
			con.setReadTimeout(300000); 
			// 数据提交方式为get
			if(token != null){
				con.setRequestProperty("Authorization",token);
				con.setRequestProperty("Access-Control-Allow-Origin","*");
			}

			// 使用输入流获取接口返回结果, 并返回给调用方
			InputStream in = con.getInputStream();
	
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
			while ((sc = reader.readLine()) != null) {
				st.append(sc);
			}

			return st.toString();
		} catch (Exception ex) {
			//logger.error(ex.getMessage());
			throw ex;
		}

	}
	
	public static String getToken (String serviceName,Date timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		return MD5.MD5Encode(serviceName+sdf.format(timestamp)+ConstantUtil.VERIFY_CODE);
	}
	public static String getToken2(Date timestamp,String customerid,String ygbm) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		return MD5.MD5Encode(sdf.format(timestamp)+customerid+ygbm+ConstantUtil.VERIFY_CODE);
	}
	public static void main(String[] args) {
		Date now = new  Date();
		JSONObject rtnMsg = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
		String token = MD5.MD5Encode("GetCaseContractStatus"+sdf.format(now)+"cloudcccrm");
		try {
			
			String str = "serviceName=GetCaseContractStatus&timestamp="+sdf.format(now)+"&verifyCode="+token+"&data={\"CaseNo\":\"900201909190001\"}";
			System.out.println(str);
			String rst = operate("http://dcm.ricoh.com.cn:9082/ricohcrmdev/distributor.action", str);
			JSONObject callRst = JSONObject.fromObject(rst);
			String SuccessFlag = (String)callRst.get("SuccessFlag");
			if(!"1".equals(SuccessFlag)) {
				rtnMsg.put("successFlag", "False");
				rtnMsg.put("Message", callRst.get("ErrorMessage"));
			}
			
			if (callRst.containsKey("ReturnData")) {
				String ReturnData = (String) callRst.get("ReturnData");
				ReturnData = ReturnData.replaceAll("null","\"\"");
				JSONObject rtnData = JSONObject.fromObject(ReturnData);
				String contStatus = (String)rtnData.get("ContStatus");
				if ("完成".equals(contStatus)) {
	    				rtnMsg.put("successFlag", "False");
	    				rtnMsg.put("Message", "合同状态已完成，请勿提交或修改报价单");
	    			}
			}
			System.out.println(rst);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
