package com.cloudcc.boot.utils.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: LGH
 * @Date: 2021/8/19 0019 16:48
 * 按当前日期取是本年第几周
 */
public class Ahou {
    public static void main(String[] args) {
        String today = "2021-08-23";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = format.parse(today);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        // 这里记得要-2 原因如下备注
        System.out.println((calendar.get(Calendar.WEEK_OF_YEAR)) - 2);
    }
}
