package com.cloudcc.boot.utils.wsdl;

public class TIPTOPServiceGateWayPortTypeProxy implements TIPTOPServiceGateWayPortType {
  private String _endpoint = null;
  private TIPTOPServiceGateWayPortType tIPTOPServiceGateWayPortType = null;
  
  public TIPTOPServiceGateWayPortTypeProxy() {
    _initTIPTOPServiceGateWayPortTypeProxy();
  }
  
  public TIPTOPServiceGateWayPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initTIPTOPServiceGateWayPortTypeProxy();
  }
  
  private void _initTIPTOPServiceGateWayPortTypeProxy() {
    try {
      tIPTOPServiceGateWayPortType = (new TIPTOPServiceGateWayLocator()).getTIPTOPServiceGateWayPortType();
      if (tIPTOPServiceGateWayPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)tIPTOPServiceGateWayPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)tIPTOPServiceGateWayPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (tIPTOPServiceGateWayPortType != null)
      ((javax.xml.rpc.Stub)tIPTOPServiceGateWayPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public TIPTOPServiceGateWayPortType getTIPTOPServiceGateWayPortType() {
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType;
  }
  
  public GetFQCDataResponse_GetFQCDataResponse getFQCData(GetFQCDataRequest_GetFQCDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getFQCData(parameters);
  }
  
  public GetWODataResponse_GetWODataResponse getWOData(GetWODataRequest_GetWODataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getWOData(parameters);
  }
  
  public GetWOStockQtyResponse_GetWOStockQtyResponse getWOStockQty(GetWOStockQtyRequest_GetWOStockQtyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getWOStockQty(parameters);
  }
  
  public CreateWOStockinDataResponse_CreateWOStockinDataResponse createWOStockinData(CreateWOStockinDataRequest_CreateWOStockinDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createWOStockinData(parameters);
  }
  
  public GetJointProductDataResponse_GetJointProductDataResponse getJointProductData(GetJointProductDataRequest_GetJointProductDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getJointProductData(parameters);
  }
  
  public GetMoMasterDataResponse_GetMoMasterDataResponse getMoMasterData(GetMoMasterDataRequest_GetMoMasterDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMoMasterData(parameters);
  }
  
  public GetAreaDataResponse_GetAreaDataResponse getAreaData(GetAreaDataRequest_GetAreaDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAreaData(parameters);
  }
  
  public GetAreaListResponse_GetAreaListResponse getAreaList(GetAreaListRequest_GetAreaListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAreaList(parameters);
  }
  
  public GetAxmDocumentResponse_GetAxmDocumentResponse getAxmDocument(GetAxmDocumentRequest_GetAxmDocumentRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAxmDocument(parameters);
  }
  
  public GetPurchaseStockInQtyResponse_GetPurchaseStockInQtyResponse getPurchaseStockInQty(GetPurchaseStockInQtyRequest_GetPurchaseStockInQtyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPurchaseStockInQty(parameters);
  }
  
  public GetBasicCodeDataResponse_GetBasicCodeDataResponse getBasicCodeData(GetBasicCodeDataRequest_GetBasicCodeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getBasicCodeData(parameters);
  }
  
  public GetComponentrepsubDataResponse_GetComponentrepsubDataResponse getComponentrepsubData(GetComponentrepsubDataRequest_GetComponentrepsubDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getComponentrepsubData(parameters);
  }
  
  public SelCardInfoResponse_SelCardInfoResponse selCardInfo(SelCardInfoRequest_SelCardInfoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.selCardInfo(parameters);
  }
  
  public CheckDataNumExistsResponse_CheckDataNumExistsResponse checkDataNumExists(CheckDataNumExistsRequest_CheckDataNumExistsRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkDataNumExists(parameters);
  }
  
  public GetAxmPriceResponse_GetAxmPriceResponse getAxmPrice(GetAxmPriceRequest_GetAxmPriceRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAxmPrice(parameters);
  }
  
  public GetPOSKeyResponse_GetPOSKeyResponse getPOSKey(GetPOSKeyRequest_GetPOSKeyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPOSKey(parameters);
  }
  
  public UndoCreateStockInDataResponse_UndoCreateStockInDataResponse undoCreateStockInData(UndoCreateStockInDataRequest_UndoCreateStockInDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.undoCreateStockInData(parameters);
  }
  
  public UndoCreateWOWorkReportDataResponse_UndoCreateWOWorkReportDataResponse undoCreateWOWorkReportData(UndoCreateWOWorkReportDataRequest_UndoCreateWOWorkReportDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.undoCreateWOWorkReportData(parameters);
  }
  
  public GetCostGroupDataResponse_GetCostGroupDataResponse getCostGroupData(GetCostGroupDataRequest_GetCostGroupDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCostGroupData(parameters);
  }
  
  public CreateSalesOrderDataResponse_CreateSalesOrderDataResponse createSalesOrderData(CreateSalesOrderDataRequest_CreateSalesOrderDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createSalesOrderData(parameters);
  }
  
  public GetWOIssueDataResponse_GetWOIssueDataResponse getWOIssueData(GetWOIssueDataRequest_GetWOIssueDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getWOIssueData(parameters);
  }
  
  public UpdateWOIssueDataResponse_UpdateWOIssueDataResponse updateWOIssueData(UpdateWOIssueDataRequest_UpdateWOIssueDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.updateWOIssueData(parameters);
  }
  
  public GetCountryDataResponse_GetCountryDataResponse getCountryData(GetCountryDataRequest_GetCountryDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCountryData(parameters);
  }
  
  public GetCountryListResponse_GetCountryListResponse getCountryList(GetCountryListRequest_GetCountryListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCountryList(parameters);
  }
  
  public GetCurrencyDataResponse_GetCurrencyDataResponse getCurrencyData(GetCurrencyDataRequest_GetCurrencyDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCurrencyData(parameters);
  }
  
  public GetCurrencyListResponse_GetCurrencyListResponse getCurrencyList(GetCurrencyListRequest_GetCurrencyListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCurrencyList(parameters);
  }
  
  public GetCustListResponse_GetCustListResponse getCustList(GetCustListRequest_GetCustListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustList(parameters);
  }
  
  public GetCustomerDataResponse_GetCustomerDataResponse getCustomerData(GetCustomerDataRequest_GetCustomerDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustomerData(parameters);
  }
  
  public GetCustomerProductDataResponse_GetCustomerProductDataResponse getCustomerProductData(GetCustomerProductDataRequest_GetCustomerProductDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustomerProductData(parameters);
  }
  
  public GetDepartmentDataResponse_GetDepartmentDataResponse getDepartmentData(GetDepartmentDataRequest_GetDepartmentDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getDepartmentData(parameters);
  }
  
  public GetDepartmentListResponse_GetDepartmentListResponse getDepartmentList(GetDepartmentListRequest_GetDepartmentListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getDepartmentList(parameters);
  }
  
  public GetPOReceivingOutDataResponse_GetPOReceivingOutDataResponse getPOReceivingOutData(GetPOReceivingOutDataRequest_GetPOReceivingOutDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPOReceivingOutData(parameters);
  }
  
  public GetEmployeeDataResponse_GetEmployeeDataResponse getEmployeeData(GetEmployeeDataRequest_GetEmployeeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getEmployeeData(parameters);
  }
  
  public GetEmployeeListResponse_GetEmployeeListResponse getEmployeeList(GetEmployeeListRequest_GetEmployeeListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getEmployeeList(parameters);
  }
  
  public GetInspectionDataResponse_GetInspectionDataResponse getInspectionData(GetInspectionDataRequest_GetInspectionDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getInspectionData(parameters);
  }
  
  public CreatePurchaseStockOutResponse_CreatePurchaseStockOutResponse createPurchaseStockOut(CreatePurchaseStockOutRequest_CreatePurchaseStockOutRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPurchaseStockOut(parameters);
  }
  
  public GetItemListResponse_GetItemListResponse getItemList(GetItemListRequest_GetItemListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemList(parameters);
  }
  
  public GetLocationDataResponse_GetLocationDataResponse getLocationData(GetLocationDataRequest_GetLocationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getLocationData(parameters);
  }
  
  public GetMonthListResponse_GetMonthListResponse getMonthList(GetMonthListRequest_GetMonthListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMonthList(parameters);
  }
  
  public GetOperationDataResponse_GetOperationDataResponse getOperationData(GetOperationDataRequest_GetOperationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOperationData(parameters);
  }
  
  public GetOverdueAmtDetailDataResponse_GetOverdueAmtDetailDataResponse getOverdueAmtDetailData(GetOverdueAmtDetailDataRequest_GetOverdueAmtDetailDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOverdueAmtDetailData(parameters);
  }
  
  public GetOverdueAmtRankingDataResponse_GetOverdueAmtRankingDataResponse getOverdueAmtRankingData(GetOverdueAmtRankingDataRequest_GetOverdueAmtRankingDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOverdueAmtRankingData(parameters);
  }
  
  public GetProdClassListResponse_GetProdClassListResponse getProdClassList(GetProdClassListRequest_GetProdClassListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProdClassList(parameters);
  }
  
  public GetProductClassDataResponse_GetProductClassDataResponse getProductClassData(GetProductClassDataRequest_GetProductClassDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProductClassData(parameters);
  }
  
  public GetSOInfoDataResponse_GetSOInfoDataResponse getSOInfoData(GetSOInfoDataRequest_GetSOInfoDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSOInfoData(parameters);
  }
  
  public GetSOInfoDetailDataResponse_GetSOInfoDetailDataResponse getSOInfoDetailData(GetSOInfoDetailDataRequest_GetSOInfoDetailDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSOInfoDetailData(parameters);
  }
  
  public GetSalesDetailDataResponse_GetSalesDetailDataResponse getSalesDetailData(GetSalesDetailDataRequest_GetSalesDetailDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSalesDetailData(parameters);
  }
  
  public GetSalesStatisticsDataResponse_GetSalesStatisticsDataResponse getSalesStatisticsData(GetSalesStatisticsDataRequest_GetSalesStatisticsDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSalesStatisticsData(parameters);
  }
  
  public GetSupplierDataResponse_GetSupplierDataResponse getSupplierData(GetSupplierDataRequest_GetSupplierDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSupplierData(parameters);
  }
  
  public GetSupplierItemDataResponse_GetSupplierItemDataResponse getSupplierItemData(GetSupplierItemDataRequest_GetSupplierItemDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSupplierItemData(parameters);
  }
  
  public GetWarehouseDataResponse_GetWarehouseDataResponse getWarehouseData(GetWarehouseDataRequest_GetWarehouseDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getWarehouseData(parameters);
  }
  
  public GetItemDataResponse_GetItemDataResponse getItemData(GetItemDataRequest_GetItemDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemData(parameters);
  }
  
  public GetBOMDataResponse_GetBOMDataResponse getBOMData(GetBOMDataRequest_GetBOMDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getBOMData(parameters);
  }
  
  public GetDocumentNumberResponse_GetDocumentNumberResponse getDocumentNumber(GetDocumentNumberRequest_GetDocumentNumberRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getDocumentNumber(parameters);
  }
  
  public CreateQuotationDataResponse_CreateQuotationDataResponse createQuotationData(CreateQuotationDataRequest_CreateQuotationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createQuotationData(parameters);
  }
  
  public GetStockDataResponse_GetStockDataResponse getStockData(GetStockDataRequest_GetStockDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getStockData(parameters);
  }
  
  public GetReceivingQtyResponse_GetReceivingQtyResponse getReceivingQty(GetReceivingQtyRequest_GetReceivingQtyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getReceivingQty(parameters);
  }
  
  public GetPODataResponse_GetPODataResponse getPOData(GetPODataRequest_GetPODataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPOData(parameters);
  }
  
  public GetMFGDocumentResponse_GetMFGDocumentResponse getMFGDocument(GetMFGDocumentRequest_GetMFGDocumentRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMFGDocument(parameters);
  }
  
  public CreatePOReceivingDataResponse_CreatePOReceivingDataResponse createPOReceivingData(CreatePOReceivingDataRequest_CreatePOReceivingDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPOReceivingData(parameters);
  }
  
  public CreateIssueReturnDataResponse_CreateIssueReturnDataResponse createIssueReturnData(CreateIssueReturnDataRequest_CreateIssueReturnDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createIssueReturnData(parameters);
  }
  
  public GetPOReceivingInDataResponse_GetPOReceivingInDataResponse getPOReceivingInData(GetPOReceivingInDataRequest_GetPOReceivingInDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPOReceivingInData(parameters);
  }
  
  public CreateStockInDataResponse_CreateStockInDataResponse createStockInData(CreateStockInDataRequest_CreateStockInDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createStockInData(parameters);
  }
  
  public GetAccountSubjectDataResponse_GetAccountSubjectDataResponse getAccountSubjectData(GetAccountSubjectDataRequest_GetAccountSubjectDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAccountSubjectData(parameters);
  }
  
  public CreatePurchaseStockInResponse_CreatePurchaseStockInResponse createPurchaseStockIn(CreatePurchaseStockInRequest_CreatePurchaseStockInRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPurchaseStockIn(parameters);
  }
  
  public GetPurchaseStockOutQtyResponse_GetPurchaseStockOutQtyResponse getPurchaseStockOutQty(GetPurchaseStockOutQtyRequest_GetPurchaseStockOutQtyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPurchaseStockOutQty(parameters);
  }
  
  public CreateTransferNoteResponse_CreateTransferNoteResponse createTransferNote(CreateTransferNoteRequest_CreateTransferNoteRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createTransferNote(parameters);
  }
  
  public GetQtyConversionResponse_GetQtyConversionResponse getQtyConversion(GetQtyConversionRequest_GetQtyConversionRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getQtyConversion(parameters);
  }
  
  public GetShippingNoticeDataResponse_GetShippingNoticeDataResponse getShippingNoticeData(GetShippingNoticeDataRequest_GetShippingNoticeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getShippingNoticeData(parameters);
  }
  
  public GetSalesDocumentResponse_GetSalesDocumentResponse getSalesDocument(GetSalesDocumentRequest_GetSalesDocumentRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSalesDocument(parameters);
  }
  
  public GetShippingOrderDataResponse_GetShippingOrderDataResponse getShippingOrderData(GetShippingOrderDataRequest_GetShippingOrderDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getShippingOrderData(parameters);
  }
  
  public GetItemUnitConversionDataResponse_GetItemUnitConversionDataResponse getItemUnitConversionData(GetItemUnitConversionDataRequest_GetItemUnitConversionDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemUnitConversionData(parameters);
  }
  
  public GetGoodsStockResponse_GetGoodsStockResponse getGoodsStock(GetGoodsStockRequest_GetGoodsStockRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getGoodsStock(parameters);
  }
  
  public GetFQCMasterDataResponse_GetFQCMasterDataResponse getFQCMasterData(GetFQCMasterDataRequest_GetFQCMasterDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getFQCMasterData(parameters);
  }
  
  public CreateShippingOrderResponse_CreateShippingOrderResponse createShippingOrder(CreateShippingOrderRequest_CreateShippingOrderRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createShippingOrder(parameters);
  }
  
  public GetReasonCodeResponse_GetReasonCodeResponse getReasonCode(GetReasonCodeRequest_GetReasonCodeRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getReasonCode(parameters);
  }
  
  public GetLabelTypeDataResponse_GetLabelTypeDataResponse getLabelTypeData(GetLabelTypeDataRequest_GetLabelTypeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getLabelTypeData(parameters);
  }
  
  public GetCountingLabelDataResponse_GetCountingLabelDataResponse getCountingLabelData(GetCountingLabelDataRequest_GetCountingLabelDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCountingLabelData(parameters);
  }
  
  public UpdateCountingLabelDataResponse_UpdateCountingLabelDataResponse updateCountingLabelData(UpdateCountingLabelDataRequest_UpdateCountingLabelDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.updateCountingLabelData(parameters);
  }
  
  public CreateMISCIssueDataResponse_CreateMISCIssueDataResponse createMISCIssueData(CreateMISCIssueDataRequest_CreateMISCIssueDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createMISCIssueData(parameters);
  }
  
  public CheckExecAuthorizationResponse_CheckExecAuthorizationResponse checkExecAuthorization(CheckExecAuthorizationRequest_CheckExecAuthorizationRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkExecAuthorization(parameters);
  }
  
  public CreateStockDataResponse_CreateStockDataResponse createStockData(CreateStockDataRequest_CreateStockDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createStockData(parameters);
  }
  
  public EboGetCustDataResponse_EboGetCustDataResponse eboGetCustData(EboGetCustDataRequest_EboGetCustDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.eboGetCustData(parameters);
  }
  
  public EboGetProdDataResponse_EboGetProdDataResponse eboGetProdData(EboGetProdDataRequest_EboGetProdDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.eboGetProdData(parameters);
  }
  
  public EboGetOrderDataResponse_EboGetOrderDataResponse eboGetOrderData(EboGetOrderDataRequest_EboGetOrderDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.eboGetOrderData(parameters);
  }
  
  public RunCommandResponse_RunCommandResponse runCommand(RunCommandRequest_RunCommandRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.runCommand(parameters);
  }
  
  public CheckApsExecutionResponse_CheckApsExecutionResponse checkApsExecution(CheckApsExecutionRequest_CheckApsExecutionRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkApsExecution(parameters);
  }
  
  public GetOrganizationListResponse_GetOrganizationListResponse getOrganizationList(GetOrganizationListRequest_GetOrganizationListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOrganizationList(parameters);
  }
  
  public GetUserTokenResponse_GetUserTokenResponse getUserToken(GetUserTokenRequest_GetUserTokenRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getUserToken(parameters);
  }
  
  public CheckUserAuthResponse_CheckUserAuthResponse checkUserAuth(CheckUserAuthRequest_CheckUserAuthRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkUserAuth(parameters);
  }
  
  public GetMenuDataResponse_GetMenuDataResponse getMenuData(GetMenuDataRequest_GetMenuDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMenuData(parameters);
  }
  
  public CreateVendorDataResponse_CreateVendorDataResponse createVendorData(CreateVendorDataRequest_CreateVendorDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createVendorData(parameters);
  }
  
  public CreateBOMMasterDataResponse_CreateBOMMasterDataResponse createBOMMasterData(CreateBOMMasterDataRequest_CreateBOMMasterDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createBOMMasterData(parameters);
  }
  
  public CreateBOMDetailDataResponse_CreateBOMDetailDataResponse createBOMDetailData(CreateBOMDetailDataRequest_CreateBOMDetailDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createBOMDetailData(parameters);
  }
  
  public CreateVoucherDataResponse_CreateVoucherDataResponse createVoucherData(CreateVoucherDataRequest_CreateVoucherDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createVoucherData(parameters);
  }
  
  public GetAccountDataResponse_GetAccountDataResponse getAccountData(GetAccountDataRequest_GetAccountDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAccountData(parameters);
  }
  
  public CreateCustomerDataResponse_CreateCustomerDataResponse createCustomerData(CreateCustomerDataRequest_CreateCustomerDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createCustomerData(parameters);
  }
  
  public CreateItemMasterDataResponse_CreateItemMasterDataResponse createItemMasterData(CreateItemMasterDataRequest_CreateItemMasterDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createItemMasterData(parameters);
  }
  
  public CreateEmployeeDataResponse_CreateEmployeeDataResponse createEmployeeData(CreateEmployeeDataRequest_CreateEmployeeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createEmployeeData(parameters);
  }
  
  public CreateAddressDataResponse_CreateAddressDataResponse createAddressData(CreateAddressDataRequest_CreateAddressDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createAddressData(parameters);
  }
  
  public TIPTOPGateWayResponse_TIPTOPGateWayResponse TIPTOPGateWay(TIPTOPGateWayRequest_TIPTOPGateWayRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.TIPTOPGateWay(parameters);
  }
  
  public CreateBillingAPResponse_CreateBillingAPResponse createBillingAP(CreateBillingAPRequest_CreateBillingAPRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createBillingAP(parameters);
  }
  
  public CreateCustomerOtheraddressDataResponse_CreateCustomerOtheraddressDataResponse createCustomerOtheraddressData(CreateCustomerOtheraddressDataRequest_CreateCustomerOtheraddressDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createCustomerOtheraddressData(parameters);
  }
  
  public CreatePotentialCustomerDataResponse_CreatePotentialCustomerDataResponse createPotentialCustomerData(CreatePotentialCustomerDataRequest_CreatePotentialCustomerDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPotentialCustomerData(parameters);
  }
  
  public GetCustomerContactDataResponse_GetCustomerContactDataResponse getCustomerContactData(GetCustomerContactDataRequest_GetCustomerContactDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustomerContactData(parameters);
  }
  
  public GetCustomerOtheraddressDataResponse_GetCustomerOtheraddressDataResponse getCustomerOtheraddressData(GetCustomerOtheraddressDataRequest_GetCustomerOtheraddressDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustomerOtheraddressData(parameters);
  }
  
  public GetItemStockListResponse_GetItemStockListResponse getItemStockList(GetItemStockListRequest_GetItemStockListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemStockList(parameters);
  }
  
  public GetMFGSettingSmaDataResponse_GetMFGSettingSmaDataResponse getMFGSettingSmaData(GetMFGSettingSmaDataRequest_GetMFGSettingSmaDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMFGSettingSmaData(parameters);
  }
  
  public GetPackingMethodDataResponse_GetPackingMethodDataResponse getPackingMethodData(GetPackingMethodDataRequest_GetPackingMethodDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPackingMethodData(parameters);
  }
  
  public GetPotentialCustomerDataResponse_GetPotentialCustomerDataResponse getPotentialCustomerData(GetPotentialCustomerDataRequest_GetPotentialCustomerDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPotentialCustomerData(parameters);
  }
  
  public GetTableAmendmentDataResponse_GetTableAmendmentDataResponse getTableAmendmentData(GetTableAmendmentDataRequest_GetTableAmendmentDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getTableAmendmentData(parameters);
  }
  
  public GetTaxTypeDataResponse_GetTaxTypeDataResponse getTaxTypeData(GetTaxTypeDataRequest_GetTaxTypeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getTaxTypeData(parameters);
  }
  
  public GetUnitConversionDataResponse_GetUnitConversionDataResponse getUnitConversionData(GetUnitConversionDataRequest_GetUnitConversionDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getUnitConversionData(parameters);
  }
  
  public GetUnitDataResponse_GetUnitDataResponse getUnitData(GetUnitDataRequest_GetUnitDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getUnitData(parameters);
  }
  
  public GetReportDataResponse_GetReportDataResponse getReportData(GetReportDataRequest_GetReportDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getReportData(parameters);
  }
  
  public CRMGetCustomerDataResponse_CRMGetCustomerDataResponse CRMGetCustomerData(CRMGetCustomerDataRequest_CRMGetCustomerDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.CRMGetCustomerData(parameters);
  }
  
  public CreateCustomerContactDataResponse_CreateCustomerContactDataResponse createCustomerContactData(CreateCustomerContactDataRequest_CreateCustomerContactDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createCustomerContactData(parameters);
  }
  
  public CreateDepartmentDataResponse_CreateDepartmentDataResponse createDepartmentData(CreateDepartmentDataRequest_CreateDepartmentDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createDepartmentData(parameters);
  }
  
  public GetAccountTypeDataResponse_GetAccountTypeDataResponse getAccountTypeData(GetAccountTypeDataRequest_GetAccountTypeDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAccountTypeData(parameters);
  }
  
  public GetTransactionCategoryResponse_GetTransactionCategoryResponse getTransactionCategory(GetTransactionCategoryRequest_GetTransactionCategoryRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getTransactionCategory(parameters);
  }
  
  public GetVoucherDocumentDataResponse_GetVoucherDocumentDataResponse getVoucherDocumentData(GetVoucherDocumentDataRequest_GetVoucherDocumentDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getVoucherDocumentData(parameters);
  }
  
  public RollbackVoucherDataResponse_RollbackVoucherDataResponse rollbackVoucherData(RollbackVoucherDataRequest_RollbackVoucherDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.rollbackVoucherData(parameters);
  }
  
  public GetCardDetailDataResponse_GetCardDetailDataResponse getCardDetailData(GetCardDetailDataRequest_GetCardDetailDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCardDetailData(parameters);
  }
  
  public GetOnlineUserResponse_GetOnlineUserResponse getOnlineUser(GetOnlineUserRequest_GetOnlineUserRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOnlineUser(parameters);
  }
  
  public GetProdInfoResponse_GetProdInfoResponse getProdInfo(GetProdInfoRequest_GetProdInfoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProdInfo(parameters);
  }
  
  public GetMemberDataResponse_GetMemberDataResponse getMemberData(GetMemberDataRequest_GetMemberDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMemberData(parameters);
  }
  
  public GetMachineDataResponse_GetMachineDataResponse getMachineData(GetMachineDataRequest_GetMachineDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMachineData(parameters);
  }
  
  public GetProdRoutingDataResponse_GetProdRoutingDataResponse getProdRoutingData(GetProdRoutingDataRequest_GetProdRoutingDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProdRoutingData(parameters);
  }
  
  public GetWorkstationDataResponse_GetWorkstationDataResponse getWorkstationData(GetWorkstationDataRequest_GetWorkstationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getWorkstationData(parameters);
  }
  
  public CreateRepSubPBOMDataResponse_CreateRepSubPBOMDataResponse createRepSubPBOMData(CreateRepSubPBOMDataRequest_CreateRepSubPBOMDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createRepSubPBOMData(parameters);
  }
  
  public GetBrandDataResponse_GetBrandDataResponse getBrandData(GetBrandDataRequest_GetBrandDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getBrandData(parameters);
  }
  
  public CreateItemApprovalDataResponse_CreateItemApprovalDataResponse createItemApprovalData(CreateItemApprovalDataRequest_CreateItemApprovalDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createItemApprovalData(parameters);
  }
  
  public GetItemOtherGroupDataResponse_GetItemOtherGroupDataResponse getItemOtherGroupData(GetItemOtherGroupDataRequest_GetItemOtherGroupDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemOtherGroupData(parameters);
  }
  
  public CreateSupplierItemDataResponse_CreateSupplierItemDataResponse createSupplierItemData(CreateSupplierItemDataRequest_CreateSupplierItemDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createSupplierItemData(parameters);
  }
  
  public CreateWOWorkReportDataResponse_CreateWOWorkReportDataResponse createWOWorkReportData(CreateWOWorkReportDataRequest_CreateWOWorkReportDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createWOWorkReportData(parameters);
  }
  
  public CreateBOMDataResponse_CreateBOMDataResponse createBOMData(CreateBOMDataRequest_CreateBOMDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createBOMData(parameters);
  }
  
  public CreateShippingOrdersWithoutOrdersResponse_CreateShippingOrdersWithoutOrdersResponse createShippingOrdersWithoutOrders(CreateShippingOrdersWithoutOrdersRequest_CreateShippingOrdersWithoutOrdersRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createShippingOrdersWithoutOrders(parameters);
  }
  
  public GetItemGroupDataResponse_GetItemGroupDataResponse getItemGroupData(GetItemGroupDataRequest_GetItemGroupDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getItemGroupData(parameters);
  }
  
  public GetProdStateResponse_GetProdStateResponse getProdState(GetProdStateRequest_GetProdStateRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProdState(parameters);
  }
  
  public GetPaymentTermsDataResponse_GetPaymentTermsDataResponse getPaymentTermsData(GetPaymentTermsDataRequest_GetPaymentTermsDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPaymentTermsData(parameters);
  }
  
  public GetSSOKeyResponse_GetSSOKeyResponse getSSOKey(GetSSOKeyRequest_GetSSOKeyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSSOKey(parameters);
  }
  
  public CreateECNDataResponse_CreateECNDataResponse createECNData(CreateECNDataRequest_CreateECNDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createECNData(parameters);
  }
  
  public CreatePLMBOMDataResponse_CreatePLMBOMDataResponse createPLMBOMData(CreatePLMBOMDataRequest_CreatePLMBOMDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPLMBOMData(parameters);
  }
  
  public GetQuotationDataResponse_GetQuotationDataResponse getQuotationData(GetQuotationDataRequest_GetQuotationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getQuotationData(parameters);
  }
  
  public GetCustClassificationDataResponse_GetCustClassificationDataResponse getCustClassificationData(GetCustClassificationDataRequest_GetCustClassificationDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustClassificationData(parameters);
  }
  
  public GetInvoiceTypeListResponse_GetInvoiceTypeListResponse getInvoiceTypeList(GetInvoiceTypeListRequest_GetInvoiceTypeListRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getInvoiceTypeList(parameters);
  }
  
  public GetTradeTermDataResponse_GetTradeTermDataResponse getTradeTermData(GetTradeTermDataRequest_GetTradeTermDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getTradeTermData(parameters);
  }
  
  public SyncAccountDataResponse_SyncAccountDataResponse syncAccountData(SyncAccountDataRequest_SyncAccountDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.syncAccountData(parameters);
  }
  
  public GetCustomerAccAmtDataResponse_GetCustomerAccAmtDataResponse getCustomerAccAmtData(GetCustomerAccAmtDataRequest_GetCustomerAccAmtDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCustomerAccAmtData(parameters);
  }
  
  public GetDataCountResponse_GetDataCountResponse getDataCount(GetDataCountRequest_GetDataCountRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getDataCount(parameters);
  }
  
  public GetSODataResponse_GetSODataResponse getSOData(GetSODataRequest_GetSODataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getSOData(parameters);
  }
  
  public GetShappingDataResponse_GetShappingDataResponse getShappingData(GetShappingDataRequest_GetShappingDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getShappingData(parameters);
  }
  
  public GetUserDefOrgResponse_GetUserDefOrgResponse getUserDefOrg(GetUserDefOrgRequest_GetUserDefOrgRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getUserDefOrg(parameters);
  }
  
  public CheckCardResponse_CheckCardResponse checkCard(CheckCardRequest_CheckCardRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkCard(parameters);
  }
  
  public CheckCardTypeResponse_CheckCardTypeResponse checkCardType(CheckCardTypeRequest_CheckCardTypeRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkCardType(parameters);
  }
  
  public CheckCouponResponse_CheckCouponResponse checkCoupon(CheckCouponRequest_CheckCouponRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkCoupon(parameters);
  }
  
  public CheckMemberUpgradeResponse_CheckMemberUpgradeResponse checkMemberUpgrade(CheckMemberUpgradeRequest_CheckMemberUpgradeRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkMemberUpgrade(parameters);
  }
  
  public GetCardScoreResponse_GetCardScoreResponse getCardScore(GetCardScoreRequest_GetCardScoreRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCardScore(parameters);
  }
  
  public GetCashCardInfoResponse_GetCashCardInfoResponse getCashCardInfo(GetCashCardInfoRequest_GetCashCardInfoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCashCardInfo(parameters);
  }
  
  public RechargeCardResponse_RechargeCardResponse rechargeCard(RechargeCardRequest_RechargeCardRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.rechargeCard(parameters);
  }
  
  public ReturnCardResponse_ReturnCardResponse returnCard(ReturnCardRequest_ReturnCardRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.returnCard(parameters);
  }
  
  public DeductSPaymentResponse_DeductSPaymentResponse deductSPayment(DeductSPaymentRequest_DeductSPaymentRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.deductSPayment(parameters);
  }
  
  public ReturnOrderBillResponse_ReturnOrderBillResponse returnOrderBill(ReturnOrderBillRequest_ReturnOrderBillRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.returnOrderBill(parameters);
  }
  
  public ChangeCardResponse_ChangeCardResponse changeCard(ChangeCardRequest_ChangeCardRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.changeCard(parameters);
  }
  
  public MemberUpgradeResponse_MemberUpgradeResponse memberUpgrade(MemberUpgradeRequest_MemberUpgradeRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.memberUpgrade(parameters);
  }
  
  public GetOrderInfoResponse_GetOrderInfoResponse getOrderInfo(GetOrderInfoRequest_GetOrderInfoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getOrderInfo(parameters);
  }
  
  public RollbackBillingAPResponse_RollbackBillingAPResponse rollbackBillingAP(RollbackBillingAPRequest_RollbackBillingAPRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.rollbackBillingAP(parameters);
  }
  
  public GetAPCategoryAccountCodeResponse_GetAPCategoryAccountCodeResponse getAPCategoryAccountCode(GetAPCategoryAccountCodeRequest_GetAPCategoryAccountCodeRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getAPCategoryAccountCode(parameters);
  }
  
  public DeletePLMTempTableDataResponse_DeletePLMTempTableDataResponse deletePLMTempTableData(DeletePLMTempTableDataRequest_DeletePLMTempTableDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.deletePLMTempTableData(parameters);
  }
  
  public GetPLMTempTableDataStatusResponse_GetPLMTempTableDataStatusResponse getPLMTempTableDataStatus(GetPLMTempTableDataStatusRequest_GetPLMTempTableDataStatusRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getPLMTempTableDataStatus(parameters);
  }
  
  public CreatePLMTempTableDataResponse_CreatePLMTempTableDataResponse createPLMTempTableData(CreatePLMTempTableDataRequest_CreatePLMTempTableDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.createPLMTempTableData(parameters);
  }
  
  public SelRepairCardResponse_SelRepairCardResponse selRepairCard(SelRepairCardRequest_SelRepairCardRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.selRepairCard(parameters);
  }
  
  public GetMemoDataResponse_GetMemoDataResponse getMemoData(GetMemoDataRequest_GetMemoDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMemoData(parameters);
  }
  
  public ChangeCardActivateResponse_ChangeCardActivateResponse changeCardActivate(ChangeCardActivateRequest_ChangeCardActivateRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.changeCardActivate(parameters);
  }
  
  public RepairCardSaleResponse_RepairCardSaleResponse repairCardSale(RepairCardSaleRequest_RepairCardSaleRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.repairCardSale(parameters);
  }
  
  public ReturnEInvoiceBooksResponse_ReturnEInvoiceBooksResponse returnEInvoiceBooks(ReturnEInvoiceBooksRequest_ReturnEInvoiceBooksRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.returnEInvoiceBooks(parameters);
  }
  
  public GetEInvoiceBooksResponse_GetEInvoiceBooksResponse getEInvoiceBooks(GetEInvoiceBooksRequest_GetEInvoiceBooksRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getEInvoiceBooks(parameters);
  }
  
  public GetARListDataResponse_GetARListDataResponse getARListData(GetARListDataRequest_GetARListDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getARListData(parameters);
  }
  
  public GetProjectWBSDataResponse_GetProjectWBSDataResponse getProjectWBSData(GetProjectWBSDataRequest_GetProjectWBSDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getProjectWBSData(parameters);
  }
  
  public CheckGiftNoResponse_CheckGiftNoResponse checkGiftNo(CheckGiftNoRequest_CheckGiftNoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.checkGiftNo(parameters);
  }
  
  public DeductGiftNOResponse_DeductGiftNOResponse deductGiftNO(DeductGiftNORequest_DeductGiftNORequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.deductGiftNO(parameters);
  }
  
  public DeductMoneyResponse_DeductMoneyResponse deductMoney(DeductMoneyRequest_DeductMoneyRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.deductMoney(parameters);
  }
  
  public GetClassDataResponse_GetClassDataResponse getClassData(GetClassDataRequest_GetClassDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getClassData(parameters);
  }
  
  public DeductScoreResponse_DeductScoreResponse deductScore(DeductScoreRequest_DeductScoreRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.deductScore(parameters);
  }
  
  public GetMemberCardInfoResponse_GetMemberCardInfoResponse getMemberCardInfo(GetMemberCardInfoRequest_GetMemberCardInfoRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getMemberCardInfo(parameters);
  }
  
  public GetScoreResponse_GetScoreResponse getScore(GetScoreRequest_GetScoreRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getScore(parameters);
  }
  
  public ModPassWordResponse_ModPassWordResponse modPassWord(ModPassWordRequest_ModPassWordRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.modPassWord(parameters);
  }
  
  public WritePointResponse_WritePointResponse writePoint(WritePointRequest_WritePointRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.writePoint(parameters);
  }
  
  public GetExchangeRateDataResponse_GetExchangeRateDataResponse getExchangeRateData(GetExchangeRateDataRequest_GetExchangeRateDataRequest parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getExchangeRateData(parameters);
  }
  
  public GetKHaimm221Response_GetKHaimm221Response getKHaimm221(GetKHaimm221Request_GetKHaimm221Request parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getKHaimm221(parameters);
  }
  
  public GetXSDDaxmt410Response_GetXSDDaxmt410Response getXSDDaxmt410(GetXSDDaxmt410Request_GetXSDDaxmt410Request parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getXSDDaxmt410(parameters);
  }
  
  public GetCHDaxmt620Response_GetCHDaxmt620Response getCHDaxmt620(GetCHDaxmt620Request_GetCHDaxmt620Request parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getCHDaxmt620(parameters);
  }
  
  public GetXSKPaxmt670Response_GetXSKPaxmt670Response getXSKPaxmt670(GetXSKPaxmt670Request_GetXSKPaxmt670Request parameters) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.getXSKPaxmt670(parameters);
  }
  
  public String invokeSrv(String request) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.invokeSrv(request);
  }
  
  public String callbackSrv(String request) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.callbackSrv(request);
  }
  
  public String syncProd(String request) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.syncProd(request);
  }
  
  public String invokeMdm(String request) throws java.rmi.RemoteException{
    if (tIPTOPServiceGateWayPortType == null)
      _initTIPTOPServiceGateWayPortTypeProxy();
    return tIPTOPServiceGateWayPortType.invokeMdm(request);
  }
  
  
}