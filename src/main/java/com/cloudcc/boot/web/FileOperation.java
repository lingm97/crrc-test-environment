package com.cloudcc.boot.web;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import com.cloudcc.boot.model.FileRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import static com.cloudcc.boot.core.CCService.userInfo;

@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "" ,tags = {"Rustful"},description = "")
public class FileOperation {
    @Value("${system.localurl:''}")
    public String localurl;
    @ResponseBody
    @RequestMapping(value = "/getFileId", produces = "text/html;charset=UTF-8")
    @ApiOperation(value = "")
    public String FileUpload(FileRequest userRequest, HttpServletRequest request) throws Exception {

        String userRquestStr = JSONObject.fromObject(userRequest).toString();//入参
//        log.info("getFileId请求参数{}", userRquestStr);
        JSONObject returnJson = new JSONObject();
        CCService cs = new CCService(userInfo);

        if(userRequest.getFile() == null){
            returnJson.put("result", "false");
            returnJson.put("returnInfo", "文件为空！");
            returnJson.put("returnCode", "-1");
            return returnJson+"";
        }
        CCObject interfacerecordObj = new CCObject("interfacerecord");//接口同步记录
        interfacerecordObj.put("url", localurl + "/api/getFileId");//url
        interfacerecordObj.put("crcs", userRquestStr);//传入参数
        try {
            String fileName = userRequest.getFile().getOriginalFilename();
            int index = fileName.lastIndexOf(".");
            if(index == -1){
                returnJson.put("result", "false");
                returnJson.put("returnInfo", "文件名错误");
                returnJson.put("returnCode", "-1");
                return returnJson+"";
            }
            String suffix = fileName.substring(index,fileName.length());
            userRequest.setFileName(fileName);
            userRequest.setSuffix(suffix);
            String relStr = cs.CC_File_Post(cs.filepath, userRequest);

            returnJson = JSONObject.fromObject(relStr);

            interfacerecordObj.put("zxzt", "处理成功");//执行状态
        } catch (Exception e) {
            returnJson.put("result", "false");
            returnJson.put("returnInfo", e + "");
            returnJson.put("returnCode", "-1");

            interfacerecordObj.put("zxzt", "处理失败");//执行状态
        } finally {
            interfacerecordObj.put("fhcs", returnJson.toString());//返回参数
//            log.error("返回参数{}", returnJson.toString());
            //写入数据 newObj
            ServiceResult sr = cs.insert(interfacerecordObj);
        }
        return returnJson+"";
    }
}
