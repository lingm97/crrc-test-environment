package com.cloudcc.boot.web;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static com.cloudcc.boot.core.CCService.userInfo;

/**
 * @author lgh
 */
@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "文件下载" ,tags = {"Rustful文件下载接口"},description = "Rustful")
public class FileWeb {
    @Value("${system.url}")
    public String url;

    @ResponseBody
    @RequestMapping(value ="/getFileUrl", produces = "text/html;charset=UTF-8")
    @ApiOperation( value = "文件下载")
    public void file(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CCService cs = new CCService(userInfo);
        ServiceResult sr = new ServiceResult();
        try {
            String 	id = request.getParameter("id");//文件ID
            String userSql = "select * from File where id='"+id+"'";
            List<CCObject> rel_cqlQuery = cs.cqlQuery("File", userSql);
            log.info("文件下载--文件数据："+rel_cqlQuery);
            String fileName = "";
            for (CCObject cc : rel_cqlQuery){
                String name = cc.get("name")==null?"":cc.get("name").toString();//文件名
                String type = cc.get("type")==null?"":cc.get("type").toString();//文件后缀
                fileName = name+"."+type;
            }
            log.info("文件下载--文件名："+fileName);
            cs.Get(url+"openApi/downloadFile?id="+id,fileName,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
