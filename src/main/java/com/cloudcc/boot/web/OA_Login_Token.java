package com.cloudcc.boot.web;

import com.cloudcc.boot.core.CCObject;
import com.cloudcc.boot.core.CCService;
import com.cloudcc.boot.core.ServiceResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static com.cloudcc.boot.core.CCService.userInfo;

/**
 * @author lgh
 */
@Controller
@RequestMapping(value = "/api")
@Slf4j
public class OA_Login_Token {
    @Value("${system.url}")
    public String url;
    @Value("${system.APPID_dd}")
    private String APPID; //单点登陆appid
    @Value("${system.oaurl}")
    private String addrss;

    @ResponseBody
    @RequestMapping(value ="/getOALoginToken", produces = "text/html;charset=UTF-8")
    @ApiOperation( value = "")
    public String getOALoginToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CCService cs = new CCService(userInfo);
        JSONObject returnJson = new JSONObject();
        CCObject interfacerecordObj = new CCObject("interfacerecord");//接口同步记录
        try {
            String 	userid = request.getParameter("userid");//当前登陆用户ID
            String loginid = getLoginid(userid);
            String uslPath = addrss+"/ssologin/getToken?appid="+APPID+"&loginid="+loginid;
            log.info("单点登陆获取链接======="+uslPath);
            String loginToken = cs.sendGet(uslPath);
            log.info("单点登陆返回token======="+loginToken);

            returnJson.put("result", "true");
            returnJson.put("returnInfo", loginToken);

            interfacerecordObj.put("url", uslPath);//url
            interfacerecordObj.put("crcs", "appid="+APPID+"&loginid="+loginid);//传入参数
            interfacerecordObj.put("zxzt", "处理成功");//执行状态
        } catch (Exception e) {
            e.printStackTrace();
            returnJson.put("result", "false");
            returnJson.put("returnInfo", e + "");
            returnJson.put("returnCode", "-1");

            log.info("========================");
            log.info("报错信息：" + e.toString());
        } finally {
            interfacerecordObj.put("fhcs", returnJson.toString());//返回参数
            //写入数据 newObj
            ServiceResult sr = cs.insert(interfacerecordObj);
        }
        return returnJson.toString();
    }

    private String getLoginid(String userid) {
        CCService cs = new CCService(userInfo);
        String loginid = "";
        List<CCObject> userList = cs.cquery("ccuser", "id='" + userid + "'");
        if (userList.size() > 0) {
            CCObject userObj = userList.get(0);
            loginid = userObj.get("loginname") == null ? "" : userObj.get("loginname").toString();//用户名（员工编号）
        }
        return loginid;
    }
}
