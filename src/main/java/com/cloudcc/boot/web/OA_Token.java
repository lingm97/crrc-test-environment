package com.cloudcc.boot.web;


import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
@Component
public class OA_Token {
    private final Map<String,String> SYSTEM_CACHE = new HashMap<>();

    @Value("${system.APPID_lc}")
    private String APPID;//流程创建及组织架构
    @Value("${system.oaurl}")
    private String addrss;
    /**
     * 第一步：
     *
     * 调用ecology注册接口,根据appid进行注册,将返回服务端公钥和Secret信息
     */
    public Map<String,Object> Regist(String address){

        //获取当前系统RSA加密的公钥
        RSA rsa = new RSA();
        String publicKey = rsa.getPublicKeyBase64();
        String privateKey = rsa.getPrivateKeyBase64();
        System.out.println("privateKey==========："+privateKey);
        System.out.println("APPID==========："+APPID);
        System.out.println("cpk==========："+publicKey);
        // 客户端RSA私钥
        SYSTEM_CACHE.put("LOCAL_PRIVATE_KEY",privateKey);
        // 客户端RSA公钥
        SYSTEM_CACHE.put("LOCAL_PUBLIC_KEY",publicKey);

        //调用ECOLOGY系统接口进行注册
        String data = HttpRequest.post(address + "/api/ec/dev/auth/regist")
                .header("appid",APPID)
                .header("cpk",publicKey)
                .timeout(2000)
                .execute().body();

        // 打印ECOLOGY响应信息
        System.out.println("Regist()："+data);
        Map<String,Object> datas = JSONUtil.parseObj(data);

        //ECOLOGY返回的系统公钥
        SYSTEM_CACHE.put("SERVER_PUBLIC_KEY",StrUtil.nullToEmpty((String)datas.get("spk")));
        //ECOLOGY返回的系统密钥
        SYSTEM_CACHE.put("SERVER_SECRET",StrUtil.nullToEmpty((String)datas.get("secrit")));
        return datas;
    }

    /**
     * 第二步：
     *
     * 通过第一步中注册系统返回信息进行获取token信息
     */
    public String getoken(){
        // 从系统缓存或者数据库中获取ECOLOGY系统公钥和Secret信息
        String secret = SYSTEM_CACHE.get("SERVER_SECRET");
        String spk = SYSTEM_CACHE.get("SERVER_PUBLIC_KEY");

        // 如果为空,说明还未进行注册,调用注册接口进行注册认证与数据更新
        if (Objects.isNull(secret)||Objects.isNull(spk)){
            Regist(addrss);
            // 重新获取最新ECOLOGY系统公钥和Secret信息
            secret = SYSTEM_CACHE.get("SERVER_SECRET");
            spk = SYSTEM_CACHE.get("SERVER_PUBLIC_KEY");
        }
        System.out.println("spk==========:"+spk);
        // 公钥加密,所以RSA对象私钥为null
        RSA rsa = new RSA(null,spk);
        //对秘钥进行加密传输，防止篡改数据
        String encryptSecret = rsa.encryptBase64(secret,CharsetUtil.CHARSET_UTF_8,KeyType.PublicKey);

        //调用ECOLOGY系统接口进行注册
        String data = HttpRequest.post(addrss+ "/api/ec/dev/auth/applytoken")
                .header("appid",APPID)
                .header("secret",encryptSecret)
                .header("time","3600")
                .execute().body();

        System.out.println("getoken()："+data);
        Map<String,Object> datas = JSONUtil.parseObj(data);
        String oa_token = StrUtil.nullToEmpty((String)datas.get("token"));
        //ECOLOGY返回的token
        // TODO 为Token缓存设置过期时间
        SYSTEM_CACHE.put("SERVER_TOKEN",StrUtil.nullToEmpty((String)datas.get("token")));

        return oa_token;
    }

    /**
     * 第三步：
     *
     * 调用ecology系统的rest接口，请求头部带上token和用户标识认证信息
     *
     * @param oa_path ecology系统地址+rest api 接口地址
     * @param userid OA经办人ID
     * @param jsonParams 请求参数json串
     *
     * 注意：ECOLOGY系统所有POST接口调用请求头请设置 "Content-Type","application/x-www-form-urlencoded; charset=utf-8"
     */
    public String osSendPost(String oa_path,String userid,String jsonParams){
        getoken();
        //ECOLOGY返回的token
        String token= SYSTEM_CACHE.get("SERVER_TOKEN");
        if (StrUtil.isEmpty(token)){
            token = (String) getoken();
        }

        String spk = SYSTEM_CACHE.get("SERVER_PUBLIC_KEY");
        //封装请求头参数
        RSA rsa = new RSA(null,spk);
        //对用户信息进行加密传输,暂仅支持传输OA用户ID
        String encryptUserid = rsa.encryptBase64(userid,CharsetUtil.CHARSET_UTF_8,KeyType.PublicKey);
//       System.out.println("encryptUserid:"+encryptUserid);
        //调用ECOLOGY系统接口
        String data = HttpRequest.post(oa_path)
                .header("appid",APPID)
                .header("token",token)
                .header("userid",encryptUserid)
                .body(jsonParams)
                .execute().body();
//        System.out.println("osSendPost()："+data);
        return data;
    }

    public static void main(String[] args) {
//       testRestful("http://10.10.10.40","/api/system/appmanage/route",null);
//	   System.out.println("spk:"+testRegist(addrss).get("spk"));
//	   System.out.println("secrit:"+testRegist(addrss).get("secrit"));
//	   String oa_token = getoken(addrss);
//        System.out.println("oa_token："+oa_token);

//	   Map<String,Object> parm = new HashMap<String,Object>();
        /*JSONObject parm = new JSONObject();
        JSONArray jsonarr = new JSONArray();
        JSONObject jsonobj1 = new JSONObject();
        jsonobj1.put("fieldName", "sqr");
        jsonobj1.put("fieldValue", "2362");
        JSONObject jsonobj2 = new JSONObject();
        jsonobj2.put("fieldName", "sqrq");
        jsonobj2.put("fieldValue", "2024-06-03");
        JSONObject jsonobj3 = new JSONObject();
        jsonobj3.put("fieldName", "xmmc");
        jsonobj3.put("fieldValue", "测试接口推送01");
        JSONObject jsonobj4 = new JSONObject();
        JSONArray jsonarr_file = new JSONArray();
        JSONObject jsonobj_file = new JSONObject();
        jsonobj_file.put("filePath", "http://172.28.188.171:55556/crm/api/getFileUrl?id=bda2024629F28A1oFsL1");
        jsonobj_file.put("fileName", "test.xlsx");
        jsonarr_file.add(jsonobj_file);
        jsonobj4.put("fieldName", "fj");
        jsonobj4.put("fieldValue", jsonarr_file);
        jsonarr.add(jsonobj1);
        jsonarr.add(jsonobj2);
        jsonarr.add(jsonobj3);
        jsonarr.add(jsonobj4);
        parm.put("mainData", jsonarr);
        parm.put("workflowId", "203");
        parm.put("requestName", "技术协议审批流程-周鹏飞-2024-06-03");
        System.out.println("para:"+parm);
        osSendPost(addrss,"/api/workflow/paService/doCreateRequest",parm.toString());*/
    }

}
