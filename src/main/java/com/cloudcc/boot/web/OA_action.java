package com.cloudcc.boot.web;

import com.cloudcc.boot.utils.util.DesUtils;
import com.cloudcc.boot.utils.util.MD5;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Random;

import static com.cloudcc.boot.utils.util.MD5.MD5Encode;


/**
 * author : 林贵美 on 2021-08-18
 * 单点登陆
 * */

@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "" ,tags = {"Rustful"},description = "")
public class OA_action {
	public static String url;
	@Value("${system.url}")
	public  void setUrl(String url) {
		this.url = url;
	}
	public static String loginurl;
	@Value("${system.loginurl}")
	public  void setLoginurl(String loginurl) {
		this.loginurl = loginurl;
	}

	private OA_LoginService oa_login_service = new OA_LoginService();
	@ResponseBody
	@RequestMapping(value ="/login", produces = "text/html;charset=UTF-8")
	@ApiOperation( value = "")
	public String oa_login(HttpServletRequest request,HttpServletResponse response){
		JSONObject returncontent = new JSONObject();
		Log log = LogFactory.getLog(getClass());
		log.info("单点登录Action进入开始");

		JSONObject rtJson = null;
		String servicename = request.getParameter("servicename")==null?"":request.getParameter("servicename").toString();//服务名
		String login_name = request.getParameter("login_name")==null?"":request.getParameter("login_name").toString();//用户名
		String login_pwd = request.getParameter("login_pwd")==null?"":request.getParameter("login_pwd").toString();//密码
		login_pwd = MD5Encode (login_pwd);
		try {
			rtJson = oa_login_service.intocrm(login_name,login_pwd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("单点登录Service返回信息"+rtJson);
		String SuccessFlag = rtJson.getString("SuccessFlag");
		if(SuccessFlag.equals("success")){
			String accessToken = rtJson.getString("accessToken");
			try {
				String re_url = loginurl+"?binding="+accessToken;
				response.sendRedirect(re_url);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			returncontent.put("result","true");
			returncontent.put("returncode","S");
			returncontent.put("returninfo","");
			returncontent.put("data","success");
		} else if(SuccessFlag.equals("1")){
			try {
				// 设置重定向的目标页面
				String targetURL = "/";
				String ErrorMessage = rtJson.getString("ErrorMessage");
				Cookie myCookie = new Cookie("message", ErrorMessage);
				myCookie.setMaxAge(3600); // 设置Cookie有效期为1小时
				myCookie.setPath("/"); // 设置Cookie在整个网站有效
				response.addCookie(myCookie);
				response.sendRedirect(targetURL);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			returncontent.put("result","true");
			returncontent.put("returncode","S");
			returncontent.put("returninfo","");
			returncontent.put("data","success");
		} else{
			//reMsg.put("fhxx", rtJson.getString("ErrorMessage"));
			returncontent.put("result","false");
			returncontent.put("returncode","E001");
			returncontent.put("returninfo","登陆失败,"+rtJson.getString("ErrorMessage")+"！");
			returncontent.put("data","");
		}
		return returncontent.toString();
	}
}