package com.cloudcc.boot.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/api")
@Slf4j
@Api(value = "" ,tags = {"Rustful"},description = "")
public class TestClass {

    @ResponseBody
    @RequestMapping(value ="/test", produces = "text/html;charset=UTF-8")
    @ApiOperation( value = "")
    public String test(HttpServletRequest request,HttpServletResponse response){
        String name = request.getParameter("name")==null?"":request.getParameter("name").toString();//name
        String date = request.getParameter("date")==null?"":request.getParameter("date").toString();//date
        JSONObject json = new JSONObject();
        json.put("name", name);
        json.put("date", date);
        return json.toString();
    }
}
